<?php //template name: Tin tức ?>
<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/news/bg.jpg)"><h1>Tin tức Sự kiện</h1></div>
<div class="content-page">
<div class="bg-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg-news.jpg)"></div>
<div class="news-list">
<div class="scrollB">
<div class="link-page current">
<a href="ha-noi-keu-goi-dau-tu-150000-ty-dong-vao-4-du-an-duong-sat.html"  data-name="2" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL">
<div class="new-icon"></div><div class="pic-thumb"><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/bts-bangkok-3.jpg"  alt="Hà Nội kêu gọi đầu tư 150.000 tỷ đồng vào 4 dự án đường sắt"></div>
<h3>Hà Nội kêu gọi đầu tư 150.000 tỷ đồng vào 4 dự án đường sắt</h3>
</a>
</div>
</div>
</div>
<div class="colum-box-news">
<div class="scrollC">
<div class="news-content">
	<div class="news-text fadein">
<h3>Hà Nội kêu gọi đầu tư 150.000 tỷ đồng vào 4 dự án đường sắt</h3>
<p></p><p>Các tuyến đường sắt đô thị Hà Nội số 3, số 4, số 5 và số 6 được kêu gọi đầu tư theo hình thức PPP có tổng mức đầu tư khoảng 150.000 tỷ đồng.</p><p><br></p><p><img src="http://mydinhpearl.com.vn/pictures/catalog/bts-bangkok-3.jpg" class=""><br><span style="font-style: italic; line-height: 1.42857;">TP Hà Nội kêu gọi đầu tư PPP vào dự án đường sắt đô thị tuyến số 6 nối giữa Trung tâm Hà Nội đến Sân bay Nội Bài. (Ảnh minh họa)</span><br></p><p><br></p><p>Trong danh mục dự kiến kêu gọi đầu tư theo hình thức đối tác công tư (PPP) trên địa bàn TP Hà Nội giai đoạn 2016 - 2020, TP Hà Nội dự kiến có tổng số 52 dự án với tổng mức đầu tư dự kiến vào khoảng 338.725 tỷ đồng.
</p><p>
<span style="line-height: 1.42857;">Đáng chú ý, trong số 35 dự án kêu gọi đầu tư theo hình thức PPP thuộc lĩnh vực hạ tầng kỹ thuật, có 4 dự án đường sắt đô thị trên cao mới được xếp vào danh mục các dự án trọng điểm giai đoạn 2016 - 2020 với tổng mức đầu tư vào khoảng 150.000 tỷ đồng (tương đương 7,1 tỷ USD).&nbsp;</span></p><p>
<span style="line-height: 1.42857;">Theo đó, TP Hà Nội kêu gọi đầu tư vào dự án tuyến đường sắt đô thị Hà Nội số 3 (đoạn từ Ga Hà Nội - Hoàng Mai) nhằm phát triển mạng lưới đường sắt đô thị, phục vụ nhu cầu vận tải hành khách khu vực nội đô và giảm ùn tắc giao thông. Dự kiến quy mô tuyến đường sắt này sẽ có chiều dài 8 km bao gồm 3 km đi ngầm với 7 ga. Dự án có tổng mức đầu tư vào khoảng 28.175 tỷ đồng.&nbsp;</span></p><p>
<span style="line-height: 1.42857;">TP Hà Nội cũng muốn kêu gọi đầu tư theo hình thức PPP vào tuyến đường sắt đô thị Hà Nội số 5, nhằm nâng cao năng lực vận tải khu vực đô thị trung tâm và đô thị vệ tinh Hòa Lạc, đồng thời thúc đẩy phát triển đô thị vệ tinh và các khu vực hai bên Đại lộ Thăng Long. Dự kiến, tuyến đường sắt này sẽ có chiều dài 38,4 km với tổng mức đầu tư lên tới 65.572 tỷ đồng.&nbsp;</span></p><p>
<span style="line-height: 1.42857;">Tuyến đường sắt đô thị số 4 giai đoạn 1 (từ Liên Hà – Vĩnh Tuy) dài 18 km gồm 6 km đi ngầm có tổng mức đầu tư 40.885 tỷ đồng là dự án tiếp theo được TP Hà Nội kêu gọi đầu tư trong giai đoạn này. Tuyến đường này sẽ phục vụ nhu cầu đi lại giữa các khu vực phát triển đô thị trong đô thị trung tâm.&nbsp;</span></p><p>
<span style="line-height: 1.42857;">Đặc biệt, nhằm phục vụ và nâng cao năng lực đáp ứng nhu cầu đi lại giữa Trung tâm Hà Nội và Cảng Hàng không Quốc tế Nội Bài, TP Hà Nội kêu gọi đầu tư PPP vào dự án đường sắt đô thị tuyến số 6 nối giữa Trung tâm Hà Nội đến Sân bay Nội Bài. Dự kiến, dự án tuyến đường sắt này sẽ có tổng chiều dài 47 km nhưng là tuyến đường đôi khổ 1.435 mm có tổng mức đầu tư vào khoảng 14.282 tỷ đồng.&nbsp;</span></p><p>
<span style="line-height: 1.42857;">UBND TP Hà Nội cho biết, dự án đường sắt đô thị tuyến số 6 nối giữa Trung tâm Hà Nội đến Sân bay Nội Bài khi hoàn thành còn thúc đẩy phát triển đô thị khu vực Bắc sông Hồng và thúc đẩy phát triển kinh tế - xã hội.</span></p><p><span style="line-height: 1.42857;"><br></span></p><p style="text-align: right; "><span style="line-height: 1.42857; font-weight: bold;">Nguyễn Quỳnh
</span></p><p style="text-align: right; "><span style="line-height: 1.42857; font-style: italic;">VOV
</span></p></div>
</div>
</div>
</div>
</div>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>