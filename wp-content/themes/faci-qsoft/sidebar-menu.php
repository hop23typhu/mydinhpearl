<div class="navigation">
	<div class="nav">
		<?php  
			   /**
				* Displays a navigation menu
				* @param array $args Arguments
				*/
				$args = array(
					'theme_location' => 'primary-menu',
					'container' => '',
					'items_wrap' => '<ul>%3$s</ul>',
				);
				wp_nav_menu( $args );
		?>
	</div>
<span></span>
</div>