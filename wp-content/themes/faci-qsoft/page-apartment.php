<?php //template name: Thông tin căn hộ ?>
<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/bg.jpg)"><h1>Thông tin căn hộ</h1></div>
<div class="content-page">
<div class="apartment">
<div class="compass"></div>
<div class="typical">
<div class="typical-top">
<a class="thumb-01" href="thong-tin-can-ho/pearl-01"  data-info="160"><div class="num-block">PEARL 01</div></a>
<a class="thumb-02" href="thong-tin-can-ho/pearl-02"  data-info="161"><div class="num-block">PEARL 02</div></a>
</div>
<div class="note-apartment">[ Chọn vào Tháp xem thông tin ]</div>
</div>
<div class="apartment-bg"></div>
</div>
<div class="info-block">
<div class="typical-name" data-block="160"><a  href="thong-tin-can-ho/pearl-01" ><div class="num">01</div><h3>Tháp <strong>PEARL 01</strong></h3><span class="shape"></span></a></div>
<div class="typical-name" data-block="161"><a  href="thong-tin-can-ho/pearl-02" ><div class="num">02</div><h3>Tháp <strong>PEARL 02</strong></h3><span class="shape"></span></a></div>
</div>
</div>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>