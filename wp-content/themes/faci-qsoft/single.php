<?php get_header(); ?>
<!--CONTAINER-->
<div class="container" >
<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/news/bg.jpg)"><h1>Tin tức Sự kiện</h1></div>
<div class="content-page">
<div class="bg-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg-news.jpg)"></div>
<div class="news-list">
<div class="scrollB">
<div class="link-page current">
<a href="<?php bloginfo('wpurl'); ?>/tin-tuc.html"  data-name="1" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL">
<div class="new-icon"></div><div class="pic-thumb"><?php the_post_thumbnail( 'full' ); ?></div>
<h3><?php the_title(); ?></h3>
</a>
</div>
</div>
</div>
<div class="colum-box-news fadeinup">
<div class="scrollC">
<div class="news-content">

</div>
</div>
</div>
</div>
<!--BOTTOM-->
<?php get_sidebar('bottom'); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>