<?php //template name: Tiện ích ?>
<?php get_header(); ?>                                                                            
<!--CONTAINER-->
<div class="container">
<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/bg.jpg)"><h1>Tiện ích dự án</h1></div>
<div class="content-page">
<a class="view-pic" href="javascript:void(0);">Hình ảnh Tiện ích</a>
<div class="facilities-full">
<div class="compass"></div>
<div class="facilities-typical">
<div class="dot-top">
<a class="dot c-01" href="javascript:void(0);"  data-box="" data-show="01">01</a>
<a class="dot c-02" href="javascript:void(0);"  data-box="" data-show="02">02</a>
<a class="dot c-03" href="javascript:void(0);"  data-box="" data-show="03">03</a>
<a class="dot c-04" href="javascript:void(0);"  data-box="" data-show="04">04</a>
<a class="dot c-04-1" href="javascript:void(0);"  data-box="" data-show="04">04</a>
<a class="dot c-05" href="javascript:void(0);"  data-box="" data-show="05">05</a>
<a class="dot c-05-1" href="javascript:void(0);"  data-box="" data-show="05">05</a>
<a class="dot c-06" href="javascript:void(0);"  data-box="" data-show="06">06</a>
<a class="dot c-07" href="javascript:void(0);"  data-box="" data-show="07">07</a>
<a class="dot c-08" href="javascript:void(0);"  data-box="" data-show="08">08</a>
<a class="dot c-08-1" href="javascript:void(0);"  data-box="" data-show="08">08</a>
<a class="dot c-09" href="javascript:void(0);"  data-box="" data-show="09">09</a>
<a class="dot c-10" href="javascript:void(0);"  data-box="" data-show="10">10</a>
<a class="dot c-10-1" href="javascript:void(0);"  data-box="" data-show="10">10</a>
<a class="dot c-11" href="javascript:void(0);"  data-box="" data-show="11">11</a>
<a class="dot c-12" href="javascript:void(0);"  data-box="" data-show="12">12</a>
<a class="dot c-13" href="javascript:void(0);"  data-box="" data-show="13">13</a>
<a class="dot c-14" href="javascript:void(0);"  data-box="" data-show="14">14</a>
<a class="dot c-15" href="javascript:void(0);"  data-box="" data-show="15">15</a>
<a class="dot c-16" href="javascript:void(0);"  data-box="" data-show="16">16</a>
<a class="dot c-17" href="javascript:void(0);"  data-box="" data-show="17">17</a>
<a class="dot c-18" href="javascript:void(0);"  data-box="" data-show="18">18</a>
<a class="dot c-19" href="javascript:void(0);"  data-box="" data-show="19">19</a>
<a class="dot c-19-1" href="javascript:void(0);"  data-box="" data-show="19">19</a>
<a class="dot c-20" href="javascript:void(0);"  data-box="" data-show="20">20</a>
<a class="dot c-21" href="javascript:void(0);"  data-box="" data-show="21">21</a>
<a class="dot c-21-1" href="javascript:void(0);"  data-box="" data-show="21">21</a>
<a class="dot c-21-2" href="javascript:void(0);"  data-box="" data-show="21">21</a>
<a class="dot c-22" href="javascript:void(0);"  data-box="" data-show="22">22</a>
<a class="dot c-23" href="javascript:void(0);"  data-box="" data-show="23">23</a>
<a class="dot c-23-1" href="javascript:void(0);"  data-box="" data-show="23">23</a>
<a class="dot c-23-2" href="javascript:void(0);"  data-box="" data-show="23">23</a>
<a class="dot c-23-3" href="javascript:void(0);"  data-box="" data-show="23">23</a>
<a class="dot c-24" href="javascript:void(0);"  data-box="" data-show="24">24</a>
<a class="dot c-25" href="javascript:void(0);"  data-box="" data-show="25">25</a>
<a class="dot c-26" href="javascript:void(0);"  data-box="" data-show="26">26</a>
<a class="dot c-27" href="javascript:void(0);"  data-box="" data-show="27">27</a>
<a class="dot c-28" href="javascript:void(0);"  data-box="" data-show="28">28</a>
<a class="dot c-29" href="javascript:void(0);"  data-box="" data-show="29">29</a>
<a class="dot c-30" href="javascript:void(0);"  data-box="" data-show="30">30</a>
</div>
</div>
<div class="facilities-bg"></div>
</div>
<div class="description-faci">
<div class="faci-box">
<div class="description-slide item-wrapper">
<div class="note-facilities item-container">
<ul>
<li data-text="01"><h3>01.Lối vào</h3></li>
<li data-text="02"><h3>02.Nhà bảo vệ</h3></li>
<li data-text="03"><h3>03.Lối ra vào tầng hầm</h3></li>
<li data-text="04"><h3>04.Lối vào khu căn hộ</h3></li>
<li data-text="05"><h3>05.Vườn hoa</h3></li>
<li data-text="06"><h3>06.Khu vực kinh doanh ngoài trời</h3></li>
<li data-text="07"><h3>07.Khu vực nghỉ chân</h3></li>
<li data-text="08"><h3>08.Khu vui chơi trẻ em</h3></li>
<li data-text="09"><h3>09.Vườn hoa đào</h3></li>
<li data-text="10"><h3>10.Sân tập Golf</h3></li>
</ul>
</div>
<div class="note-facilities item-container">
<ul>
<li data-text="11"><h3>11.Cafe ngoài trời</h3></li>
<li data-text="12"><h3>12.Không gian mở đa chức năng</h3></li>
<li data-text="13"><h3>13.Khu vực nướng ngoài trời</h3></li>
<li data-text="14"><h3>14.Khu vực thể dục thể thao ngoài trời</h3></li>
<li data-text="15"><h3>15.Sân Tennis</h3></li>
<li data-text="16"><h3>16.Hồ bơi người lớn</h3></li>
<li data-text="17"><h3>17.Hồ bơi trẻ em</h3></li>
<li data-text="18"><h3>18.Quầy giải khát hồ bơi</h3></li>
<li data-text="19"><h3>19.Sàn phơi nắng hồ bơi</h3></li>
<li data-text="20"><h3>20.Bể sục</h3></li>
</ul>
</div>
<div class="note-facilities item-container">
<ul>
<li data-text="21"><h3>21.Lối thoát hiểm</h3></li>
<li data-text="22"><h3>22.Thang DV hồ bơi &amp; thoát hiểm tầng hầm</h3></li>
<li data-text="23"><h3>23.Bãi đậu xe</h3></li>
<li data-text="24"><h3>24.Cafe</h3></li>
<li data-text="25"><h3>25.Gym &amp; Spa</h3></li>
<li data-text="26"><h3>26.Nhà trẻ</h3></li>
<li data-text="27"><h3>27.Trực phòng cháy chữa cháy</h3></li>
<li data-text="28"><h3>28.Shop</h3></li>
<li data-text="29"><h3>29.Shop</h3></li>
<li data-text="30"><h3>30.Sinh hoạt cộng đồng</h3></li>
</ul>
</div>
</div>
<div class="pagination"></div>
</div>
</div>
<div class="facilities-pic">
<a class="close-content" href="javascript:void(0);">Close</a>
<div class="details-box">
<div  class="item-box" >
<div class="text-name">
<h3>Bể bơi</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/1_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/1_l.jpg"  alt="Bể bơi">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Đường đi dạo</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/2_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/2_s.jpg"  alt="Đường đi dạo">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Phòng tập Gym</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/3_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/3_s.jpg"  alt="Phòng tập Gym">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Khu vui chơi</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/4_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/4_s.jpg"  alt="Khu vui chơi">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Nhà trẻ</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/5_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/5_s.jpg"  alt="Nhà trẻ">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Nhà hàng</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/6_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/6_s.jpg"  alt="Nhà hàng">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Nhà hàng</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/7_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/7_s.jpg"  alt="Nhà hàng">
</div>
</div>
<div  class="item-box" >
<div class="text-name">
<h3>Sân Tennis</h3></div>
<div class="pic-box">
<a class="zoom" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/8_l.jpg" ></a>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/facilities/8_s.jpg"  alt="Sân Tennis">
</div>
</div>
</div>
</div>
</div>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>