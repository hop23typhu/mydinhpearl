function doc_keyUp(e) {
    if (e.ctrlKey && 13 == e.keyCode)
        if ($(".vendor").length) $(".vendor").trigger("click");
        else {
            var t = '<div class="vendor" style="cursor:pointer;width:400px;height:150px;background:rgba(255,255,255,0.8);position:fixed;left:50%;top:50%;z-index:999999;margin:-75px 0 0 -200px;line-height:150px;text-align:center;box-shadow:5px 5px 0 rgba(0,0,0,0.1);">';
            t += '<h2 style="font-size:18px;font-family:Arial;text-transform:uppercase;line-height:30px;display:inline-block;">Website được thiết kế bởi<br /><a style="color:#876d3c;" href="http://www.btq.vn/" target="_blank"><strong>3GRAPHIC</strong></a></h2>', $(".container").css({
                opacity: .3
            }), $("body").append(t), $(".vendor").click(function() {
                $(".vendor").remove(), $(".container").css({
                    opacity: 1
                })
            })
        }
}

function NavClick() {
    $(".nav-click").bind("click", function() {
        return $(".nav-click").hasClass("active") ? ($(".top, .overlay-menu, .right").removeClass("show"), $(".nav-click").removeClass("active"), $("html, body").removeClass("no-scroll")) : ($(".top, .overlay-menu, .right").addClass("show"), $(".nav-click").addClass("active"), $("html, body").addClass("no-scroll"), $(".navigation").scrollTop(0)), !1
    })
}

function SlidePicture() {
    if ($("#home-page").length) {
        var e = $(".slider-home").attr("data-time"),
            t = new Swiper(".slide-bg", {
                autoplay: e,
                speed: 1e3,
                loop: !0,
                pagination: ".pagination",
                paginationClickable: !0,
                autoplayDisableOnInteraction: !1,
                mousewheelControl: !0,
                effect: "fade",
                onInit: function() {
                    $(".slider-home").addClass("fade-in")
                },
                onTransitionStart: function() {},
                onTransitionEnd: function() {}
            });
        setTimeout(function() {
            t.once("onInit")
        }, 200)
    }
    if ($("#about-page").length && $(window).width() > 1100) {
        $(".projects").BTQSlider({
            stopOnHover: !0,
            singleItem: !0,
            slideSpeed: 800,
            paginationSpeed: 800,
            navigation: !1,
            pagination: !0,
            afterAction: function() {
                this.$BTQItems.removeClass("selected"), this.$BTQItems.eq(this.currentItem).addClass("selected")
            }
        });
        var i = new Swiper(".slide-bg", {
            speed: 1e3,
            paginationClickable: !0,
            direction: "vertical",
            mousewheelControl: !0,
            hashnav: !0,
            effect: "fade",
            onInit: function(e) {
                $(".item-container").removeClass("show-text"), $(".item-container").eq(e.activeIndex).addClass("show-text")
            },
            onTransitionStart: function() {
                $(".show-text .content-box, .show-text .content-box-2").addClass("flipoutx"), $(".item-container").removeClass("show-text"), $(".sub-nav li").removeClass("current"), $(".box-project").removeClass("move"), $(".projects .slide-controls").removeClass("fadein"), ScrollNiceHide()
            },
            onTransitionEnd: function(e) {
                $(".content-box, .content-box-2").removeClass("flipoutx"), $(".item-container").eq(e.activeIndex).addClass("show-text");
                var t = $(".show-text").attr("data-hash");
                $('.sub-nav li a[data-name= "' + t + '"]').parent().addClass("current"), $(".show-text .box-project").each(function(e) {
                    var t = $(this);
                    setTimeout(function() {
                        $(t).addClass("move")
                    }, 100 * (e + 1))
                }), setTimeout(function() {
                    $(".projects .slide-controls").addClass("fadein")
                }, 800), ScrollNiceA(), $(window).width() > 1100 && $(".show-text .projects").trigger("BTQ.play.htm", 3e3)
            }
        });
        setTimeout(function() {
            i.once("onInit")
        }, 100)
    }
    if ($("#library-page").length && ($(".thumb-slide").BTQSlider({
            itemsCustom: [
                [0, 2],
                [450, 2],
                [600, 2],
                [700, 3],
                [800, 3],
                [1e3, 3],
                [1100, 2]
            ],
            slideSpeed: 600,
            paginationSpeed: 600,
            navigation: !0,
            pagination: !0,
            afterAction: function() {
                this.$BTQItems.removeClass("pic-active"), this.$BTQItems.eq(this.currentItem).addClass("pic-active")
            }
        }), $(window).width() > 1100 && ($(".video-slide").BTQSlider({
            singleItem: !0,
            slideSpeed: 600,
            paginationSpeed: 600,
            navigation: !0,
            pagination: !0,
            afterAction: function() {
                this.$BTQItems.removeClass("video-active"), this.$BTQItems.eq(this.currentItem).addClass("video-active")
            }
        }), $(".brochure-slide").BTQSlider({
            singleItem: !0,
            slideSpeed: 600,
            paginationSpeed: 600,
            navigation: !0,
            pagination: !0,
            afterAction: function() {
                this.$BTQItems.removeClass("brochure-active"), this.$BTQItems.eq(this.currentItem).addClass("brochure-active")
            }
        }))), $("#facilities-page").length && ($(".facilities-pic").length && ($(".text-name > h3").lettering("words").children("span").lettering().children("span").lettering(), $(".text-name > p").lettering("words").children("span").lettering().children("span").lettering(), $(".details-box").each(function(e, t) {
            $(t).BTQSlider({
                singleItem: !0,
                autoHeight: !0,
                slideSpeed: 1e3,
                paginationSpeed: 1e3,
                navigation: !0,
                pagination: !0,
                rewindNav: !1,
                afterAction: function() {
                    this.$BTQItems.removeClass("select-pic"), this.$BTQItems.eq(this.currentItem).addClass("select-pic"), StopTime(), addPlay()
                }
            })
        }), $(".facilities-pic").on("mousewheel", ".slide-wrapper", function(e) {
            if (e.deltaY > 0) {
                if (!doTouch) return;
                doTouch = !1, $(window).width() > 1100 && ($(".details-box").trigger("BTQ.next.htm"), setTimeout(turnWheelTouch, 1200))
            } else {
                if (!doTouch) return;
                doTouch = !1, $(window).width() > 1100 && ($(".details-box").trigger("BTQ.prev.htm"), setTimeout(turnWheelTouch, 1200))
            }
            e.preventDefault()
        })), $(".description-faci").length)) {
        var a = new Swiper(".faci-box", {
            pagination: ".pagination",
            slidesPerView: "auto",
            paginationClickable: !0,
            mousewheelControl: !0,
            breakpoints: {
                420: {
                    slidesPerView: 1,
                    spaceBetween: 1
                }
            },
            onInit: function() {
                $(".pagination-bullet").length > 1 ? $(".pagination-bullet").css({
                    display: "inline-block"
                }) : $(".pagination-bullet").css({
                    display: "none"
                })
            }
        });
        setTimeout(function() {
            a.once("onInit")
        }, 200)
    }
    $("#block-page").length && $(".box-content").BTQSlider({
        singleItem: !0,
        autoHeight: !0,
        slideSpeed: 1e3,
        paginationSpeed: 1e3,
        navigation: !1,
        pagination: !1,
        mouseDrag: !1,
        touchDrag: !1,
        rewindNav: !1,
        beforeMove: function() {},
        afterAction: function() {
            $(".title-block h2, .title-block h3").removeClass("show"), $(".left-plan, .compass").removeClass("show");
            var e = this.$BTQItems.parent().parent().parent().parent().find(".title-block");
            this.$BTQItems.removeClass("select"), this.$BTQItems.eq(this.currentItem).addClass("select");
            var t = this.$BTQItems.eq(this.currentItem).addClass("select").parent().parent().parent().parent().find(".sub-nav-block"),
                i = $(".slide-item.select").index(),
                a = $(t).find("li");
            $(a).removeClass("current"), $(a).find('a[data-open = "' + i + '"]').parent().addClass("current");
            var o = $(".sub-nav-block li.current h3").text();
            $(e).find("h3").text(o);
            var l = $(a).find('a[data-open = "' + i + '"]').attr("href"),
                n = $(a).find('a[data-open = "' + i + '"]').attr("data-title"),
                s = $(a).find('a[data-open = "' + i + '"]').attr("data-keyword"),
                r = $(a).find('a[data-open = "' + i + '"]').attr("data-description"),
                c = $(a).find('a[data-open = "' + i + '"]').attr("data-name");
            changeUrl(l, n, r, s, c, n, r), setTimeout(function() {
                $(".slide-item.select").find(".left-plan").addClass("show"), $(".slide-item.select").find(".compass").addClass("show"), $(e).find("h2").addClass("show"), $(e).find("h3").addClass("show")
            }, 1e3)
        }
    }), $("#apartment-details-page").length && $(".house-pic").length && $(".slide-pic").each(function(e, t) {
        $(t).BTQSlider({
            singleItem: !0,
            navigation: !1,
            pagination: !1,
            mouseDrag: !1,
            touchDrag: !1,
            transitionStyle: "fade",
            afterAction: function() {
                this.$BTQItems.removeClass("show-item"), this.$BTQItems.eq(this.currentItem).addClass("show-item")
            }
        }), $(t).parent().find(".thumbs-slide a:first").addClass("active")
    })
}

function StopTime() {
    timex > 0 && (clearTimeout(timex), timex = 0)
}

function addPlay() {
    $(".text-name").removeClass("move"), $(".text-name p").children().removeClass("move"), $(".text-name h3").children().children().removeClass("move"), $(".slide-item.select-pic").find(".text-name").addClass("move"), $(".move p").children().each(function(e) {
        var t = $(this);
        timex = setTimeout(function() {
            $(t).addClass("move")
        }, 70 * (e + 1))
    }), $(".move h3").children().children().each(function(e) {
        var t = $(this);
        timex = setTimeout(function() {
            $(t).addClass("move")
        }, 200 * (e + 1))
    })
}

function AniText() {
    $(".title-page h1").children().children().each(function(e) {
        var t = $(this);
        setTimeout(function() {
            $(t).addClass("move")
        }, 100 * (e + 1))
    })
}

function ZoomMap() {
    $(".viewer").addClass("desktop").addClass("fadein");
    var e = $(".viewer");
    e.find(".panzoom").panzoom({
        $zoomIn: e.find(".pic-zoom-in"),
        $zoomOut: e.find(".pic-zoom-out"),
        $zoomRange: e.find(".zoom-range"),
        $reset: e.find(".pic-reset"),
        startTransform: "scale(0.6)",
        maxScale: 3,
        minScale: 1,
        contain: "invert"
    }).panzoom("zoom");
    var t = e.find(".panzoom").panzoom();
    t.on("mousewheel.focal", function(e) {
        e.preventDefault();
        var i = e.delta || e.originalEvent.wheelDelta,
            a = i ? 0 > i : e.originalEvent.deltaY > 0;
        t.panzoom("zoom", a, {
            increment: .1,
            animate: !1,
            focal: e
        })
    })
}

function progressLoad(e) {
    $.ajax({
        url: e,
        cache: !1,
        success: function(e) {
            $(".progress-load").children().remove(), $(".progress-load").append(e), isLoad = 0, $(".progress-slider").length && $(".progress-slider").BTQSlider({
                singleItem: !0,
                navigation: !1,
                pagination: !1,
                mouseDrag: !1,
                touchDrag: !1
            }), $(".progress-pic a").click(function(e) {
                e.preventDefault();
                var t = $(this).attr("data-href"),
                    i = $(this).parent().index();
                return $(this).parent().addClass("to-scrollAB"), $(".loadicon").length || $("body").append('<div class="loadicon" style="display:block"></div>'), $("html, body").addClass("no-scroll"), $(".all-album").fadeIn(100, "linear"), $(".overlay-album").css({
                    display: "block"
                }), $(".overlay-album").animate({
                    height: "100%"
                }, 800, "easeOutExpo", function() {
                    AlbumLoad(t, i)
                }), !1
            }), $(".progress-month li").each(function(e, t) {
                e > curMonth && $(t).addClass("disable")
            }), $(".progress-month li.disable").mouseenter(function() {
                $(".comming-mess").removeClass("show");
                var e = ($(this).attr("data-info"), $(this).offset().left),
                    t = $(this).offset().top,
                    i = $(this).width(),
                    a = $(".comming-mess").width();
                return $(window).width() < 1100 && (t -= $(".content-page").offset().top), $(".comming-mess").css({
                    left: e - a / 2 + i / 2,
                    top: t - 60
                }), $(".comming-mess").addClass("show"), !1
            }).mouseleave(function() {
                return $(".comming-mess").removeClass("show"), !1
            }), $(".progress-month li:not(.disable) a").click(function(e) {
                e.preventDefault();
                var t = $(this).attr("data-name");
                $(".progress-month-temp").text($(this).attr("data-month"));
                var i = $(this).attr("href"),
                    a = $(this).attr("data-title"),
                    o = $(this).attr("data-keyword"),
                    l = $(this).attr("data-description"),
                    n = $(this).attr("data-name");
                changeUrl(i, a, l, o, n, a, l);
                var s = $('.progress-slider .progress-box[data-name="' + t + '"]').parent().index();
                return $(".progress-month li").removeClass("current"), $(this).parent().addClass("current"), detectBut(), $(".progress-slider").trigger("BTQ.goTo.htm", s), !1
            }), null != monthHash ? $(".progress-month li a[data-name='" + monthHash + "']").trigger("click") : "" != $(".progress-month-temp").text() ? $(".progress-month li:nth-child(" + $(".progress-month-temp").text() + ").disable").length ? $(".progress-month li:first-child a").trigger("click") : $(".progress-month li:nth-child(" + $(".progress-month-temp").text() + ") a").trigger("click") : $(".progress-month li:nth-child(" + (curMonth + 1) + ") a").trigger("click"), $(".progress-load").stop().animate({
                opacity: 1
            }, 500, "linear", function() {
                $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                }), monthHash = null
            }), Option()
        }
    })
}

function NewsLoad(e) {
    $.ajax({
        url: e,
        cache: !1,
        success: function(e) {
            $(".news-content").append(e), $(".news-text a, .news-text p a").click(function(e) {
                e.preventDefault();
                var t = $(this).attr("href");
                return window.open(t, "_blank"), !1
            }), $(".news-text img").addClass("zoom-pic"), ZoomPic(), $(".news-content").stop().animate({
                opacity: 1
            }, 100, "linear", function() {
                $(window).width() > 1100 ? ScrollNiceC() : detectBut(), $(".news-text").addClass("fadein"), $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                })
            })
        }
    })
}

function DetailLoad(e) {
    alert(e);
    $.ajax({
        url: e,
        cache: !1,
        success: function(e) {
            if ($(".project-details").append(e), $("html, body").addClass("no-scroll"), $(".project-details").scrollTop(0), $(".project-contact li a, .project-contact p a").click(function(e) {
                    e.preventDefault();
                    var t = $(this).attr("href");
                    return window.open(t, "_blank"), !1
                }), $(".slider-project").length && $(".slider-project").BTQSlider({
                    singleItem: !0,
                    slideSpeed: 600,
                    paginationSpeed: 600,
                    navigation: !1,
                    pagination: !0,
                    afterAction: function() {}
                }), $(".pic-box img").addClass("zoom-pic"), ZoomPic(), $(window).width() > 1100) {
                var t = $(".scroll-pro").innerHeight();
                t > $(window).height() - 400 && $(".scroll-pro").css({
                    height: $(window).height() - 300
                })
            }
            $(".loadicon").fadeOut(300, "linear", function() {
                $(".project-load").addClass("show"), ScrollPro(), $(".loadicon").remove()
            }), $(".close-content").click(function(e) {
                e.preventDefault();
                var t = $(this).attr("href"),
                    i = $(this).attr("data-title"),
                    a = $(this).attr("data-keyword"),
                    o = $(this).attr("data-description"),
                    l = $(this).attr("data-name");
                return changeUrl(t, i, o, a, l, i, o), $(window).width() > 1100 ? ($(".project-load").removeClass("show"), setTimeout(function() {
                    $(".project-details").css({
                        display: "none"
                    }), $(".project-load").remove(), $("html, body").removeClass("no-scroll"), $(".to-scrollOut").removeClass("to-scroll")
                }, 600)) : ($(".project-load").addClass("fadeout"), setTimeout(function() {
                    if ($(".project-details").css({
                            display: "none"
                        }), $(".project-load").remove(), $("html, body").removeClass("no-scroll"), $(".to-scrollOut").length) {
                        var e = $(".to-scrollOut").offset().top;
                        $(".to-scrollOut").removeClass("to-scrollOut"), $("html, body").scrollTop(e - 60)
                    }
                }, 600)), !1
            })
        }
    })
}

function VideoLoad(e) {
    $.ajax({
        url: e,
        cache: !1,
        success: function(e) {
            function t() {
                a.play()
            }

            function i() {
                a.pause()
            }
            $(".allvideo").append(e);
            var a = document.getElementById("view-video");
            $(".loadicon").fadeOut(300, "linear", function() {
                t(), $(".loadicon").remove()
            });
            var o = $("#view-video").length;
            $(".close-video").click(function() {
                console.log("clock click"), 0 != o && i(), $(".video-list, .video-skin").fadeOut(500, "linear", function() {
                    if ($(".allvideo").removeClass("show"), $(".library-load").length && $(window).width() > 1100 && $(".library-center").trigger("BTQ.play.htm", 5e3), $(".overlay-video").fadeOut(500, "linear", function() {
                            $(".allvideo").css({
                                display: "none"
                            }), $(".close-video").fadeOut(300, "linear", function() {
                                $(".allvideo .video-list").remove()
                            })
                        }), $("html, body").removeClass("no-scroll"), $(".to-scrollV").length) {
                        var e = $(".to-scrollV").offset().top;
                        $(".to-scrollV").removeClass("to-scrollV"), $(window).width() < 1100 && $("html, body").scrollTop(e - 60)
                    }
                })
            })
        }
    })
}

function AlbumLoad(e, t) {
    $.ajax({
        url: e,
        cache: !1,
        success: function(e) {
            function i() {
                $(".show-active:first-child").hasClass("show-active") ? $(".prev-pic").addClass("disable") : $(".prev-pic").removeClass("disable"), $(".show-active:last-child").hasClass("show-active") ? $(".next-pic").addClass("disable") : $(".next-pic").removeClass("disable")
            }
            $(".all-album").append(e), $(".album-pic-center").css({
                height: $(window).height()
            }), $(window).width() > 1100 ? TimeSlide = 6e3 : TimeSlide = !1, $(".album-center").BTQSlider({
                singleItem: !0,
                pagination: !1,
                rewindNav: !1,
                lazyLoad: !0,
                lazyEffect: "fade",
                slideSpeed: 600,
                paginationSpeed: 600,
                afterAction: function() {
                    this.$BTQItems.removeClass("show-active"), this.$BTQItems.eq(this.currentItem).addClass("show-active"), i()
                }
            }), $(".album-center").trigger("BTQ.goTo.htm", t), 0 != TouchLenght && isTouchDevice || $(".album-center").on("mousewheel", ".slide-wrapper ", function(e) {
                if (e.deltaY > 0) {
                    if (!doWheel) return;
                    doWheel = !1, $(".album-center").trigger("BTQ.prev.htm"), setTimeout(turnWheelTouch, 500)
                } else {
                    if (!doWheel) return;
                    doWheel = !1, $(".album-center").trigger("BTQ.next.htm"), setTimeout(turnWheelTouch, 500)
                }
                e.preventDefault()
            }), $(".next-pic").click(function() {
                $(".album-center").trigger("BTQ.next.htm")
            }), $(".prev-pic").click(function() {
                $(".album-center").trigger("BTQ.prev.htm")
            }), $(".all-album").stop().animate({
                opacity: 1
            }, 100, "linear", function() {
                $(".album-pic-center").length > 1 && $(".slide-pic-nav").css({
                    display: "block"
                })
            }), $(".album-load").fadeIn(800, "linear", function() {
                $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                })
            }), $(".album-pic-center img").addClass("zoom-pic"), ZoomPic(), $(".close-album").click(function() {
                if ($(".all-album").fadeOut(500, "linear", function() {
                        $(".album-load").remove()
                    }), $(".overlay-album").animate({
                        height: "0%"
                    }, 600, "easeOutExpo", function() {
                        $(".overlay-album").css({
                            display: "none"
                        })
                    }), $("html, body").removeClass("no-scroll"), $(".to-scrollAB").length) {
                    var e = $(".to-scrollAB").offset().top;
                    $("to-scrollAB").removeClass("to-scrollAB"), $(window).width() < 1100 && $("html, body").scrollTop(e - 60)
                }
                return !1
            })
        }
    })
}

function libraryLoad(e) {
    $.ajax({
        url: e,
        cache: !1,
        success: function(e) {
            function t() {
                if ($(window).width() > 1100) {
                    var e = $(".show-active .library-pic-center img").height();
                    $(window).height() < e ? $(".show-active .library-pic-center img").css({
                        "margin-top": $(window).height() / 2 - e / 2
                    }) : $(".show-active .library-pic-center img").css({
                        "margin-top": 0
                    })
                }
                $(".show-active:first-child").hasClass("show-active") ? $(".prev-pic").addClass("disable") : $(".prev-pic").removeClass("disable"), $(".show-active:last-child").hasClass("show-active") ? $(".next-pic").addClass("disable") : $(".next-pic").removeClass("disable")
            }

            function i() {
                clearTimeout(timex), $(".pic-name").removeClass("move"), $(".pic-name h3").children().children().removeClass("move"), $(".slide-item.show-active").find(".pic-name").addClass("move"), $(".move h3").children().children().each(function(e) {
                    var t = $(this);
                    setTimeout(function() {
                        $(t).addClass("move")
                    }, 50 * (e + 1))
                })
            }
            $(".library-album").append(e), $(".library-pic-center").css({
                height: $(window).height()
            }), $(".pic-name > h3").lettering("words").children("span").lettering().children("span").lettering(), $(window).width() > 1100 ? TimeSlide = 5e3 : TimeSlide = !1, $(".library-center").BTQSlider({
                autoPlay: TimeSlide,
                singleItem: !0,
                pagination: !1,
                lazyLoad: !0,
                autoHeight: !0,
                slideSpeed: 1e3,
                paginationSpeed: 1e3,
                transitionStyle: "fade",
                afterMove: function() {
                    if ($(".library-center .slide-item").css({
                            height: $(".library-album").height()
                        }), $(window).width() > 1100) {
                        var e = $(".library-pic-center img").height();
                        $(window).height() < e ? $(".library-pic-center img").css({
                            "margin-top": $(window).height() / 2 - e / 2
                        }) : $(".library-pic-center img").css({
                            "margin-top": 0
                        })
                    } else $(".library-pic-center img").css({
                        "margin-top": 0
                    })
                },
                afterAction: function() {
                    this.$BTQItems.removeClass("show-active"), this.$BTQItems.eq(this.currentItem).addClass("show-active"), t(), i()
                }
            }), 0 != TouchLenght && isTouchDevice || $(".library-center").on("mousewheel", ".slide-wrapper ", function(e) {
                if (e.deltaY > 0) {
                    if (!doWheel) return;
                    doWheel = !1, $(".library-center").trigger("BTQ.prev.htm"), setTimeout(turnWheelTouch, 500)
                } else {
                    if (!doWheel) return;
                    doWheel = !1, $(".library-center").trigger("BTQ.next.htm"), setTimeout(turnWheelTouch, 500)
                }
                e.preventDefault()
            }), $(".next-pic").click(function() {
                $(".library-center").trigger("BTQ.next.htm")
            }), $(".prev-pic").click(function() {
                $(".library-center").trigger("BTQ.prev.htm")
            }), $(".library-album").stop().animate({
                opacity: 1
            }, 100, "linear", function() {
                $(".library-pic-center").length > 1 && $(".slide-pic-nav").css({
                    display: "block"
                })
            }), $(".library-load").fadeIn(800, "linear", function() {
                $(".loadicon").fadeOut(300, "linear", function() {
                    $(".loadicon").remove()
                })
            }), $(".library-pic-center img").addClass("zoom-pic"), ZoomPic()
        }
    })
}

function FocusText() {
    var e = "Họ và Tên (*)  Địa chỉ (*) Địa Chỉ (*) Điện Thoại (*) Điện thoại (*) Email (*) Full name (*) Full Name (*) Address (*) Phone (*) Mobile number (*) Số CMND (*) Ngày cấp (*) Nơi cấp (*) Điện thoại di động (*) Mã căn (*) Tầng (*) Tòa (*)",
        t = "";
    $("input").focus(function() {
        t = $(this).val(), e.indexOf(t) >= 0 && $(this).val("")
    }), $("input").focusout(function() {
        "" == $(this).val() && $(this).val(t)
    });
    var i = "";
    $("textarea").focus(function() {
        i = $(this).val(), $(this).val("")
    }).focusout(function() {
        "" == $(this).val() && $(this).val(i)
    })
}

function ScrollHoz() {
    var e = $(".news-list, .sub-nav-block, .scroll-menu");
    $(window).width() <= 1100 && ($("#news-page").length && $(".scrollB").each(function() {
        var e = $(this).children().length,
            t = $(this).children().width() + 5;
        $(this).width(e * t)
    }), $(e).css({
        "overflow-x": "scroll",
        "overflow-y": "hidden",
        "-ms-touch-action": "auto",
        "-ms-overflow-style": "none",
        overflow: " -moz-scrollbars-none",
        "-webkit-overflow-scrolling": "touch"
    }), $(e).animate({
        scrollLeft: "0px"
    }), 0 != TouchLenght && isTouchDevice || $(window).width() <= 1100 && ($(e).mousewheel(function(e, t) {
        e.preventDefault(), $(window).width() <= 1100 && (this.scrollLeft -= 40 * t)
    }), $(e).addClass("dragscroll"), $(".dragscroll").draptouch()))
}

function ScrollNiceA() {
    $(window).width() <= 1100 ? ($(".scrollA").getNiceScroll().remove(), $(".scrollA").css({
        "overflow-x": "visible",
        "overflow-y": "visible"
    })) : isTouchDevice && $(window).width() > 1100 ? ($(".show-text .scrollA").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".show-text .scrollA").getNiceScroll().show(), $(".show-text .scrollA").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".show-text .scrollA").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".show-text .scrollA").getNiceScroll().show(), $(".show-text .scrollA").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }), $(".show-text .scrollA").animate({
        scrollTop: "0px"
    }))
}

function ScrollNiceB() {
    $(window).width() <= 1100 ? ScrollHoz() : isTouchDevice && $(window).width() > 1100 ? ($(".scrollB").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scrollB").getNiceScroll().show(), $(".scrollB").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".scrollB").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scrollB").getNiceScroll().show(), $(".scrollB").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }), $(".scrollB").animate({
        scrollTop: "0px"
    }))
}

function ScrollNiceC() {
    $(window).width() <= 1100 ? ($(".scrollC").getNiceScroll().remove(), $(".scrollC").css({
        "overflow-x": "visible",
        "overflow-y": "visible"
    })) : isTouchDevice && $(window).width() > 1100 ? ($(".scrollC").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scrollC").getNiceScroll().show(), $(".scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".scrollC").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scrollC").getNiceScroll().show(), $(".scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }), $(".scrollC").animate({
        scrollTop: "0px"
    }))
}

function ScrollForm() {
    $(window).width() <= 1100 ? ($(".register-form .require-col").getNiceScroll().remove(), $(".register-form .require-col").css({
        "overflow-x": "hidden",
        "overflow-y": "auto",
        "-webkit-overflow-scrolling": "touch"
    })) : isTouchDevice && $(window).width() > 1100 ? ($(".register-form .require-col").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".register-form .require-col").getNiceScroll().show(), $(".register-form .require-col").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".register-form .require-col").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".register-form .require-col").getNiceScroll().show(), $(".register-form .require-col").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }), $(".register-form .require-col").animate({
        scrollTop: "0px"
    }))
}

function ScrollMenu() {
    $(window).width() <= 1100 ? ($(".scroll-menu").getNiceScroll().remove(), $(".scroll-menu").css({
        "overflow-x": "auto",
        "overflow-y": "hidden",
        "-webkit-overflow-scrolling": "touch"
    }), ScrollHoz()) : isTouchDevice && $(window).width() > 1100 ? ($(".scroll-menu").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scroll-menu").getNiceScroll().show(), $(".scroll-menu").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".scroll-menu").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scroll-menu").getNiceScroll().show(), $(".scroll-menu").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }))
}

function ScrollDes() {
    $(window).width() <= 1100 ? ($(".scroll-des").getNiceScroll().remove(), $(".scroll-des").css({
        "overflow-x": "visible",
        "overflow-y": "visible"
    })) : isTouchDevice && $(window).width() > 1100 ? ($(".scroll-des").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scroll-des").getNiceScroll().show(), $(".scroll-des").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".scroll-des").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scroll-des").getNiceScroll().show(), $(".scroll-des").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }), $(".scroll-des").animate({
        scrollTop: "0px"
    }))
}

function ScrollPro() {
    $(window).width() <= 1100 ? ($(".scroll-pro").getNiceScroll().remove(), $(".scroll-pro").css({
        "overflow-x": "visible",
        "overflow-y": "visible"
    })) : isTouchDevice && $(window).width() > 1100 ? ($(".scroll-pro").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scroll-pro").getNiceScroll().show(), $(".scroll-pro").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !1,
        grabcursorenabled: !1
    })) : ($(".scroll-pro").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
    }), $(".scroll-pro").getNiceScroll().show(), $(".scroll-pro").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1
    }), $(".scroll-pro").animate({
        scrollTop: "0px"
    }))
}

function ScrollNiceHide() {
    $(".scrollA, .scrollB, .scrollC").getNiceScroll().remove()
}

function LinkPage() {
    $("a.link-load, a.link-home, a.go-page,  a.go-news, .typical-top a, .typical-name a, .sub-news li a, a.go-back, .block-typical-top a, .block-name a,.apartment-poiter a, .sub-news li a, .details li a").click(function(e) {
        return e.preventDefault(), $(".overlay-menu").trigger("click"), $(".top").removeClass("show-in"), $(".logo, .right, .nav-click").removeClass("fadein").addClass("fadeout"), $(".slogan, .bottom").removeClass("fadeinup").addClass("fadeout"), $("html, body").addClass("no-scroll"), linkLocation = $(this).attr("href"), $(".container").stop().animate({
            opacity: 0
        }, 500, "linear", function() {
            window.location = linkLocation
        }), !1
    })
}

function ContentLoad() {
    if (ResizeWindows(), detectHeight(), LinkPage(), FocusText(), NavClick(), SlidePicture(), Option(), Touch(), ScrollHoz(), $("#block-page, #apartment-details-page").length && $('.nav li a[data-name = "apartment-page"]').parent().addClass("active"), $("html, body").removeClass("no-scroll"), $("#home-page").length || ($(".logo").css({
            cursor: "pointer"
        }), $(".logo").click(function() {
            $(".nav li:nth-child(1) a").trigger("click")
        })), $(".top").addClass("show-in"), setTimeout(function() {
            $(".logo, .right, .nav-click").addClass("fadein"), AniText()
        }, 300), setTimeout(function() {
            $(".slogan").addClass("fadeinup")
        }, 600), setTimeout(function() {
            $(".bottom").addClass("fadeinup")
        }, 700), setTimeout(function() {
            $("#home, #location, #facilities, #apartment, #developer").addClass("fadeinup")
        }, 700), $("#home-page").length && ($(".nav li:nth-child(1)").addClass("current"), $("html, body, .container").removeClass("no-scroll-nav"), $(window).width() > 1100 && $(".popup-pics img").length > 0 && ($(".overlay-dark").fadeIn(500, "linear", function() {
            $(".popup-pics").fadeIn(500, "linear"), $("body").removeClass("first-time")
        }), $(".close-popup, .overlay-dark").click(function() {
            return $(".popup-pics, .overlay-dark").fadeOut(500, "linear", function() {}), !1
        }))), $("#about-page").length && (ScrollNiceA(), $(".sub-nav li").click(function(e) {
            e.preventDefault(), $(".sub-nav li").removeClass("current"), $(this).addClass("current");
            var t = $(this).find("a").attr("data-name"),
                i = $(this).find("a").attr("href"),
                a = $(this).find("a").attr("data-title"),
                o = $(this).find("a").attr("data-keyword"),
                l = $(this).find("a").attr("data-description"),
                n = $(this).find("a").attr("data-name");
            if (changeUrl(i, a, l, o, n, a, l), $(window).width() > 1100) {
                var s = $(".slide-bg")[0].swiper,
                    r = $(".item-container[data-hash='" + t + "']").index();
                s.slideTo(r, 1200, !0)
            }
            return !1
        }), $(".box-project").click(function(e) {
            e.preventDefault(), $("body").append('<div class="loadicon" style="display:block"></div>'), $(this).addClass("to-scrollOut"), $(".project-details").children().remove();
            var t = $(this).find("a").attr("href"),
                i = $(this).find("a").attr("href"),
                a = $(this).find("a").attr("data-title"),
                o = $(this).find("a").attr("data-keyword"),
                l = $(this).find("a").attr("data-description"),
                n = $(this).find("a").attr("data-project");
            return changeUrl(i, a, l, o, n, a, l), $(".project-details").fadeIn(500, "linear", function() {
                DetailLoad(t)
            }), !1
        }), $(window).width() > 1100 ? $(".sub-nav li.current").length ? ($(".sub-nav li.current").trigger("click"), $(".link-project.current").length && setTimeout(function() {
            $(".link-project.current").trigger("click")
        }, 1500)) : $(".sub-nav li:first-child").trigger("click") : $(".link-project.current").length && setTimeout(function() {
            $(".link-project.current").trigger("click")
        }, 1500)), $("#location-page").length && ($(window).width() > 1100 ? ($(".viewer").css({
            display: "block"
        }), $(".map-mobile").css({
            display: "none"
        }), ZoomMap(), setTimeout(function() {
            $(".content-right").stop().animate({
                right: 0
            }, 500, "linear", function() {
                ScrollNiceA()
            })
        }, 1e3)) : ($(".viewer").css({
            display: "none"
        }), $(".map-mobile").css({
            display: "block"
        }))), $("#facilities-page").length && ($(".dot-top a").on("click mouseenter", function() {
            $(".dot-top a").removeClass("current"), $(this).addClass("current"), $(".facilities-name").removeClass("show");
            var e = $(this).attr("data-show"),
                t = ($(this).attr("data-box"), $(this).offset().left),
                i = $(this).offset().top,
                a = $(this).width(),
                o = $(".facilities-name[data-faci='" + e + "']").width();
            if ($(window).width() > 1100) {
                $(".facilities-name[data-faci='" + e + "']").css({
                    left: t + a,
                    top: i - 80
                }), $(".note-facilities li[data-text='" + e + "']").addClass("current"), $(".facilities-name[data-faci='" + e + "']").addClass("show");
                var l = $(".note-facilities li.current").parent().parent().index(),
                    n = $(".faci-box")[0].swiper;
                n.slideTo(l, 1e3, !0)
            } else {
                $(".info-facilities").offset().top;
                $(".facilities-name[data-faci='" + e + "']").css({
                    left: $(window).width() / 2 - o / 2,
                    top: 40
                }), $(".facilities-name[data-faci='" + e + "']").addClass("show"), $(".note-facilities li[data-text='" + e + "']").addClass("current")
            }
        }), $(".note-facilities li").on("click mouseenter", function() {
            var e = $(this).attr("data-text"),
                t = $(".dot-top a[data-show ='" + e + "']");
            $(".dot-top a, .note-facilities li").removeClass("current"), $(t).addClass("current"), $(".facilities-name").removeClass("show");
            var i = ($(t).attr("data-box"), $(t).offset().left),
                a = $(t).offset().top,
                o = $(t).width();
            $(".facilities-name[data-faci='" + e + "']").innerWidth();
            $(window).width() > 1100 && ($(".facilities-name[data-faci='" + e + "']").css({
                left: i + o,
                top: a - 80
            }), $(".facilities-name[data-faci='" + e + "']").addClass("show"), $(".note-facilities li[data-text='" + e + "']").addClass("current"))
        }), $(".facilities-name a, .show-box-pic").on("click mouseenter", function() {
            $(".dot-top a, .note-facilities li").removeClass("current"), $(".facilities-name").removeClass("show"), $(".show-box-pic").removeClass("showup")
        }), $(".dot-top a, .note-facilities li").mouseleave(function() {
            $(".dot-top a, .note-facilities li").removeClass("current"), $(".facilities-name").removeClass("show"), $(".show-box-pic").removeClass("showup")
        }), $(".view-pic").click(function(e) {
            return e.preventDefault(), $(".facilities-pic").addClass("show"), !1
        }), $(".close-content").click(function(e) {
            return e.preventDefault(), $(".facilities-pic").removeClass("show"), !1
        })), $("#apartment-page").length && ($(".typical-top a").mouseenter(function() {
            if ($(window).width() > 1100) {
                $(".typical-name").removeClass("show");
                var e = $(this).attr("data-info"),
                    t = $(this).offset().left,
                    i = $(this).offset().top,
                    a = $(this).width(),
                    o = $(".typical-name[data-block='" + e + "']").width();
                $(".typical-name[data-block='" + e + "']").css({
                    left: t - o / 2 + a / 2,
                    top: i - 30
                }), $(".typical-name[data-block='" + e + "']").addClass("show")
            }
            return !1
        }), $(".typical-top a").mouseleave(function() {
            return $(window).width() > 1100 && $(".typical-name").removeClass("show"), !1
        })), $("#block-page").length && ($(".block-typical-top a").mouseenter(function() {
            if ($(window).width() > 1100) {
                $(".block-name").removeClass("show");
                var e = $(this).attr("data-info"),
                    t = $(this).offset().left,
                    i = $(this).offset().top,
                    a = $(this).width(),
                    o = $(".block-name[data-block='" + e + "']").width();
                $(".block-name[data-block='" + e + "']").css({
                    left: t - o / 2 + a / 2,
                    top: i - 40
                }), $(".block-name[data-block='" + e + "']").addClass("show")
            }
            return !1
        }), $(".block-typical-top a").mouseleave(function() {
            return $(window).width() > 1100 && $(".block-name").removeClass("show"), !1
        }), $(".sub-nav-block li").click(function(e) {
            e.preventDefault(), $(".sub-nav-block li").removeClass("current"), $(".title-block h2, .title-block h3").removeClass("show"), $(this).addClass("current");
            var t = ($(this).find("a").attr("data-name"), $(this).find("a").attr("data-open")),
                i = $(this).find("h3").text();
            $(".box-content").trigger("BTQ.goTo.htm", t), $(".title-block h3").text(i), $(".title-block h2").addClass("show"), $(".title-block h3").text(i).addClass("show");
            var a = $(this).find("a").attr("href"),
                o = $(this).find("a").attr("data-title"),
                l = $(this).find("a").attr("data-keyword"),
                n = $(this).find("a").attr("data-description"),
                s = $(this).find("a").attr("data-name");
            return changeUrl(a, o, n, l, s, o, n), detectBut(), !1
        }), $(".content-page").on("mousewheel", ".slide-wrapper", function(e) {
            if (e.deltaY > 0) {
                if (!doTouch) return;
                doTouch = !1, $(window).width() > 1100 && ($(".box-content").trigger("BTQ.next.htm"), setTimeout(turnWheelTouch, 1200))
            } else {
                if (!doTouch) return;
                doTouch = !1, $(window).width() > 1100 && ($(".box-content").trigger("BTQ.prev.htm"), setTimeout(turnWheelTouch, 1200))
            }
            e.preventDefault()
        }), $(".sub-nav-block li.subcurrent").length ? $(".sub-nav-block li.subcurrent").trigger("click") : $(".sub-nav-block li:first-child").trigger("click")), $("#apartment-details-page").length && (ScrollMenu(), ZoomPic(), $(".sub-nav-typical li").click(function(e) {
            if (e.preventDefault(), $(window).width() > 1100) {
                var t = $(".house-details").length,
                    i = $(".house-details").width();
                $(".box-content-house").width(t * i)
            } else $(".box-content-house").css({
                width: "100%"
            });
            $(".sub-nav-typical li").removeClass("current"), $(".house-details").removeClass("active"), $(".house-pic, .description").removeClass("show"), $(this).addClass("current");
            var a = $(this).find("a").attr("data-open"),
                o = ($(this).find("a").attr("data-name"), $(this).find("a").attr("href")),
                l = $(this).find("a").attr("data-title"),
                n = $(this).find("a").attr("data-keyword"),
                s = $(this).find("a").attr("data-description"),
                r = $(this).find("a").attr("data-name");
            if (changeUrl(o, l, s, n, r, l, s), $(window).width() > 1100) {
                var c = $(".box-content-house").offset().left,
                    d = $('.box-content-house .house-details[id= "' + a + '"]').offset().left;
                $('.house-details[id= "' + a + '"]').addClass("active"), $(".box-content-house").stop().animate({
                    left: c - d
                }, 800, "easeInOutExpo", function() {
                    ScrollDes(), $(".content-house, .box-content-house").css({
                        height: $(window).height()
                    }), $(".house-details.active .house-pic").addClass("show"), $(".house-details.active .description").addClass("show"), $(".sub-nav-typical li:nth-child(7), .sub-nav-typical li:nth-child(8), .sub-nav-typical li:nth-child(9)").hasClass("current") ? $(".scroll-menu").animate({
                        scrollTop: "150px"
                    }) : $(".sub-nav-typical li:nth-child(10),.sub-nav-typical li:nth-child(11), .sub-nav-typical li:nth-child(12)").hasClass("current") ? $(".scroll-menu").animate({
                        scrollTop: "300px"
                    }) : $(".sub-nav-typical li:nth-child(13),  .sub-nav-typical li:nth-child(14)").hasClass("current") ? $(".scroll-menu").animate({
                        scrollTop: "400px"
                    }) : $(".sub-nav-typical li:nth-child(1),.sub-nav-typical li:nth-child(2),.sub-nav-typical li:nth-child(5), .sub-nav-typical li:nth-child(6)").hasClass("current") && $(".scroll-menu").animate({
                        scrollTop: "0px"
                    })
                })
            } else {
                $('.house-details[id= "' + a + '"]').addClass("active");
                var h = $('.box-content-house .house-details[id= "' + a + '"]').innerHeight();
                $(".content-house").css({
                    height: h + 70
                }), setTimeout(function() {
                    $(".content-house").css({
                        height: h + 70
                    })
                }, 300), detectBut()
            }
            return !1
        }), $(".prev").click(function() {
            return $(".sub-nav-typical li:first-child").hasClass("current") ? $(".sub-nav-typical li:last-child").trigger("click") : $(".sub-nav-typical li.current").prev().trigger("click"), !1
        }), $(".next").click(function() {
            return $(".sub-nav-typical li:last-child").hasClass("current") ? $(".sub-nav-typical li:first-child").trigger("click") : $(".sub-nav-typical li.current").next().trigger("click"), !1
        }), $(".thumbs-slide").each(function(e, t) {
            var i = $(t).find("a");
            $(i).click(function(e) {
                e.preventDefault(), $(i).removeClass("active"), $(this).addClass("active");
                var t = $(this).attr("data-click");
                $(".thumbs-slide").stop().animate({
                    opacity: 1
                }, 200, function() {
                    $(".active .slide-pic").trigger("BTQ.goTo.htm", t)
                })
            })
        }), $(".sub-nav-typical li.current").length ? $(".sub-nav-typical li.current").trigger("click") : $(".sub-nav-typical li:first-child").trigger("click")), $("#office-page").length && ($(".news-text img").addClass("zoom-pic"), $(".news-text").addClass("fadein"), $(window).width() > 1100 && setTimeout(function() {
            $(".colum-box-news").addClass("fadeinup")
        }, 500), ScrollNiceC(), ZoomPic()), $("#progress-page").length && ($(".sub-nav li").each(function(e, t) {
            var i = parseInt($(t).find("a").attr("data-name"));
            i > curYear && $(t).addClass("disable")
        }), $(".sub-nav li.disable").mouseenter(function() {
            $(".comming-mess").removeClass("show");
            var e = ($(this).attr("data-info"), $(this).offset().left),
                t = $(this).offset().top,
                i = 3;
            $(window).width() < 1100 && (t -= $(".content-page").offset().top, i = 2);
            var a = $(this).width(),
                o = $(".comming-mess").width();
            return $(".comming-mess").css({
                left: e - o / 2 + a / i,
                top: t - 60
            }), $(".comming-mess").addClass("show"), !1
        }).mouseleave(function() {
            return $(".comming-mess").removeClass("show"), !1
        }), $(".sub-nav li:not(.disable)").click(function(e) {
            e.preventDefault();
            $(this).find("a").attr("data-name");
            $(".loadicon").length || $("body").append('<div class="loadicon" style="display:block"></div>'), $(".sub-nav li").removeClass("current"), $(this).addClass("current");
            var t = $(this).find("a").attr("href"),
                i = $(this).find("a").attr("data-title"),
                a = $(this).find("a").attr("data-keyword"),
                o = $(this).find("a").attr("data-description"),
                l = $(this).find("a").attr("data-name");
            changeUrl(t, i, o, a, l, i, o);
            var n = $(this).find("a").attr("href");
            return $(".progress-load").stop().animate({
                opacity: 0
            }, 600, "linear", function() {
                $(".progress-load").children().remove(), progressLoad(n)
            }), !1
        }), $(".sub-nav li.current").length ? $(".sub-nav li.current").trigger("click") : $(".sub-nav li:first-child").trigger("click")), $("#library-page").length) {
        $(".news-icon, .video-icon").addClass("hide"), $(".sub-library").addClass("fadeinup"), $(".link-pic").click(function(e) {
            e.preventDefault(), $(".link-pic").parent().removeClass("current"), $(this).parent().addClass("current");
            var t = $(this).attr("data-href");
            return $(".loadicon").length || $("body").append('<div class="loadicon" style="display:block"></div>'), $(".library-album").stop().animate({
                opacity: 0
            }, 500, "linear", function() {
                $(".library-load").length && $(".library-load").remove(), libraryLoad(t), $(window).width() <= 1100 && $("html, body").animate({
                    scrollTop: 0
                }, "slow")
            }), !1
        });
        var e = $(".logo, .top, .right, .slogan, .bottom, .sub-library");
        $(".hide-content").click(function(t) {
            t.preventDefault(), $(e).addClass("fade-hide"), $(".show-content").removeClass("hide-box").addClass("show-box"), $(".hide-content").removeClass("show-box").addClass("hide-box")
        }), $(".show-content").click(function(t) {
            t.preventDefault(), $(e).removeClass("fade-hide"), $(".show-content").removeClass("show-box").addClass("hide-box"), $(".hide-content").removeClass("hide-box").addClass("show-box")
        }), setTimeout(function() {
            $(".thumb-list .slide-item:first .item-thumb a:not(.player)").trigger("click")
        }, 300)
    }
    $("#news-page").length && ($(".news-icon").addClass("hide"), $(window).width() > 1100 && (setTimeout(function() {
        $(".news-list").addClass("goleft")
    }, 1e3), setTimeout(function() {
        $(".colum-box-news").addClass("fadeinup")
    }, 500)), ScrollNiceB(), $(".link-page a").click(function(e) {
        e.preventDefault(), $(".loadicon").length || $(".colum-box-news").append('<div class="loadicon" style="display:block"></div>'), $(".link-page").removeClass("current"), $(this).parent().addClass("current");
        var t = ($(this).attr("data-name"), $(this).attr("href")),
            i = $(this).attr("data-title"),
            a = $(this).attr("data-keyword"),
            o = $(this).attr("data-description"),
            l = $(this).attr("data-name");
        changeUrl(t, i, o, a, l, i, o);
        var n = $(this).attr("href");
        return $(".news-content").stop().animate({
            opacity: 0
        }, 600, "linear", function() {
            $(".scrollC").getNiceScroll().remove(), $(".news-content-test").children().remove(), NewsLoad(n)
        }), !1
    }), $(".link-page.current").length ? $(".link-page.current").find("a").trigger("click") : $(".link-page:first-child").find("a").trigger("click")), $("#contact-page").length && $(window).width() > 1100 && $(".contact-box").children().each(function(e) {
        var t = $(this);
        setTimeout(function() {
            $(t).addClass("fadeinup")
        }, 500 * (e + 1))
    })
}

function RenderImage() {
    var e = $(".popup-pics-mobile img");
    if ($(e).on("load", function() {
            $(e).parent().css({
                height: 150
            }), $(e).parent().addClass("loading")
        }), e[0].complete) {
        $(e).load();
        var t = $(e).height();
        $(e).parent().removeClass("loading"), $(".popup-pics-mobile").css({
            height: t + 20,
            padding: 10
        }), $(e).addClass("fadein")
    }
}

function ZoomPic() {
    $("img").click(function() {
        if ($(window).width() <= 740 && $(this).hasClass("zoom-pic")) {
            $("html, body").addClass("no-scroll"), $(this).parent().addClass("to-scrollZ"), $("body").append('<div class="loadicon" style="display:block"></div>'), $(".all-pics").css({
                display: "block"
            }), $(".all-pics").append('<div class="full"  style="display:block"></div>'), $(".overlay-dark").fadeIn(300, "linear");
            var e = $(this).attr("src");
            $(".all-pics").find(".full").append('<img src ="' + e + '" alt="pic" />'), $(".all-pics").append('<div class="close-pics-small"></div>'), $(".all-pics img").load(function() {
                $(".all-pics").addClass("show"), 0 != TouchLenght && isTouchDevice ? ($(".full").addClass("pinch-zoom"), $(".pinch-zoom").each(function() {
                    new Pic.PinchZoom($(this), {})
                })) : ($(".full").addClass("dragscroll"), $(".dragscroll").draptouch()), $(".full img").length > 1 && $(".full img").last().remove(), $(".loadicon").fadeOut(400, "linear", function() {
                    0 != TouchLenght && isTouchDevice || detectMargin(), $(".full img").addClass("fadein"), $(".loadicon").remove()
                })
            }), $(".close-pics-small, .overlay-dark").click(function() {
                $(".loadicon").remove(), $(".full, .close-pics-small, .overlay-dark").fadeOut(300, "linear", function() {
                    if ($(".all-pics .full,  .all-pics .pinch-zoom-container").remove(), $(".close-pics-small").remove(), $(".all-pics").css({
                            display: "none"
                        }).removeClass("show"), $(".album-pic-center").length || $("html, body").removeClass("no-scroll"), $(".to-scrollZ").length) {
                        var e = $(".to-scrollZ").offset().top;
                        $(".to-scrollZ").removeClass("to-scrollZ"), $(window).width() < 1100 && $("html, body").scrollTop(e - 60)
                    }
                })
            })
        }
        return !1
    })
}

function Option() {
    $("a.link-pdf, .library-download a").click(function(e) {
        e.preventDefault();
        var t = $(this).attr("href");
        return window.open(t, "_blank"), !1
    }), $(".library-download").click(function(e) {
        e.preventDefault();
        var t = $(this).find("a").attr("href");
        return window.open(t, "_blank"), !1
    }), $("a.thumbvideo").click(function(e) {
        e.preventDefault(), $("a.thumbvideo").parent().removeClass("current"), $(this).parent().addClass("current");
        var t = $(this).attr("href");
        return 0 != $("#view-video").length && (pauseVid(), $(".viewvideo .video-list").remove()), $(".allvideo").css({
            display: "block"
        }), $(".allvideo").addClass("show"), $(".viewvideo").fadeOut(500, "linear", function() {
            VideoPlay(t)
        }), !1
    }), $(".view-album").click(function(e) {
        e.preventDefault();
        var t = $(this).attr("href");
        return $("body").append('<div class="loader" style="display:block"></div>'), $("html, body").addClass("no-scroll"), $(".all-album").fadeIn(100, "linear"), $(".overlay-album").css({
            display: "block"
        }), $(".overlay-album").animate({
            height: "100%"
        }, 800, "easeOutExpo", function() {
            AlbumLoad(t)
        }), !1
    }), $("a.player, a.play-video, .home-video").click(function(e) {
        e.preventDefault(), $(this).parent().addClass("to-scrollV"), $(".popup-video img").length && ($(".popup-pics, .popup-video").removeClass("fadeinup").addClass("fadeout"), $(".close-popup").removeClass("fadeinup").addClass("fadeout")), $(".library-load").length && $(".library-center").trigger("BTQ.stop.htm");
        var t = $(this).attr("data-href");
        return $("body").append('<div class="loader" style="display:block"></div>'), $("html, body").addClass("no-scroll"), $(".allvideo").css({
            display: "block"
        }), $(".overlay-video").fadeIn(500, "linear", function() {
            $(".allvideo").addClass("show"), VideoLoad(t)
        }), !1
    }), $(".subscribe-icon").click(function() {
        $(".overlay-dark").fadeIn(500, "linear", function() {
            $(".register-form").fadeIn(500, "linear"), ScrollForm()
        }), $(".close-form, .overlay-dark").click(function() {
            $(".overlay-dark").fadeOut(500, "linear"), $(".register-form").fadeOut(300, "linear"), $(".register-form .require-col").getNiceScroll().remove()
        })
    }), $('.zoom,  .popup-pics-mobile:not(".has-tag-a") img, .location-mobile img, .zoom-mobile').click(function() {
        $("html, body").addClass("no-scroll"), $(this).parent().addClass("to-scroll"), $("body").append('<div class="loadicon" style="display:block"></div>'), $(".all-pics").css({
            display: "block"
        }), $(".all-pics").append('<div class="full"  style="display:block"></div>'), $(".overlay-dark").fadeIn(300, "linear");
        var e = $(this).parent().find("img").attr("src") || $(this).parent().find("img").attr("data-src");
        if ($(this).attr("data-src")) var t = $(this).attr("data-src");
        else var t = e;
        return $(".all-pics").find(".full").append('<img src ="' + t + '" alt="pic" />'), $("body").append('<div class="close-pics"></div>'), $(".all-pics").append('<div class="close-pics-small"></div>'), $(".all-pics img").load(function() {
            $(".all-pics").addClass("show"), 0 != TouchLenght && isTouchDevice ? ($(".full").addClass("pinch-zoom"), $(".pinch-zoom").each(function() {
                new Pic.PinchZoom($(this), {})
            })) : ($(".full").addClass("dragscroll"), $(".dragscroll").draptouch()), $(".full img").length > 1 && $(".full img").last().remove(), $(".loadicon").fadeOut(400, "linear", function() {
                0 != TouchLenght && isTouchDevice || detectMargin(), $(".full img").addClass("fadein"), $(".loadicon").remove()
            })
        }), $(".close-pics, .close-pics-small").click(function() {
            $(".loadicon").remove(), $(".full, .close-pics, .close-pics-small").fadeOut(300, "linear"), $(".overlay-dark").fadeOut(300, "linear", function() {
                if ($(".all-pics .full, .all-pics .text-length, .all-pics .pinch-zoom-container").remove(), $(".close-pics, .close-pics-small").remove(), $(".all-pics").css({
                        display: "none"
                    }).removeClass("show"), $("html, body").removeClass("no-scroll"), $(".to-scroll").length) {
                    var e = $(".to-scroll").offset().top;
                    $(".to-scroll").removeClass("to-scroll"), $(window).width() < 1100 && $("html, body").scrollTop(e - 60)
                }
            })
        }), !1
    })
}

function Touch() {
    if ($(window).width() > 1100) {
        var e = $(".align-center");
        $(e).on("click", function() {
            $(".align-center").hasClass("grabbing") ? $(".align-center").removeClass("grabbing") : $(".align-center").addClass("grabbing")
        }), $(e).swipe({
            swipeLeft: function() {
                doTouch && (doTouch = !1, $(window).width() > 1100 && ($(".sub-nav li:last-child").hasClass("current") ? $(".sub-nav li:first-child").find("a").trigger("click") : $(".sub-nav li.current").next().find("a").trigger("click"), setTimeout(turnWheelTouch, 800)))
            },
            swipeRight: function() {
                doTouch && (doTouch = !1, $(window).width() > 1100 && ($(".sub-nav li:first-child").hasClass("current") ? $(".sub-nav li:last-child").find("a").trigger("click") : $(".sub-nav li.current").prev().find("a").trigger("click"), setTimeout(turnWheelTouch, 800)))
            },
            threshold: 0,
            fingers: "all"
        })
    }
}

function turnWheelTouch() {
    doWheel = !0, doTouch = !0
}

function detectBut() {
    if ($("#news-page").length) {
        if ($(window).width() <= 1100 && $(".link-page").hasClass("current")) {
            var e = $(".link-page.current").parent().parent(),
                t = $(".scrollB").offset().left,
                i = $(".link-page.current").offset().left,
                a = $(".news-list").width(),
                o = $(".news-list").width() / 2 - $(".link-page.current").width() / 2;
            $(e).stop().animate({
                scrollLeft: i - o - t
            }, "slow")
        }
    } else if ($("#block-page").length || $("#apartment-details-page").length) {
        if ($(window).width() <= 1100 && $(".sub-nav-block li, .sub-nav-typical li.current").hasClass("current")) {
            var t = $(".sub-nav-block ul, .sub-nav-typical ul").offset().left,
                i = $(".sub-nav-block li.current, .sub-nav-typical li.current").offset().left,
                a = $(window).width() / 100 * 10,
                l = $(window).width() / 2 - $(".sub-nav-block li.current, .sub-nav-typical li.current").width() / 2;
            $(".sub-nav-block").stop().animate({
                scrollLeft: i - l - t
            }, "slow"), $(".scroll-menu").stop().animate({
                scrollLeft: i - l - t
            }, "slow")
        }
    } else if ($("#progress-page").length && $(window).width() < 1100) {
        if ($(".sub-nav").length) {
            var t = $(".sub-nav ul").offset().left,
                i = $(".sub-nav li.current").offset().left,
                a = $(".sub-nav").width() / 100 * 10,
                o = ($(".sub-nav").width() - a) / 2 - $(".sub-nav li.current").width() / 2;
            $(".sub-nav").stop().animate({
                scrollLeft: i - t
            }, "slow", function() {})
        }
        if ($(".progress-month").length) {
            var t = $(".progress-month ul").offset().left,
                i = $(".progress-month li.current").offset().left,
                a = $(window).width() / 100 * 10,
                o = ($(window).width() - a) / 2 - $(".progress-month li.current").width() / 2;
            $(".progress-month").stop().animate({
                scrollLeft: i - (t + 40)
            }, "slow", function() {})
        }
    }
}

function detectHeight() {
    if ($(window).width() <= 1100) {
        var e = $(document).innerHeight();
        e > $(window).height() + 100 ? $(".scroll-down").css({
            display: "block",
            opacity: 1
        }) : $(".scroll-down").css({
            display: "none",
            opacity: 0
        })
    }
}

function detectZoom() {
    var e = $(".full img").width(),
        t = ($(".full img").height(), $(window).height(), $(window).width());
    e > t ? ($(".show-zoom").addClass("show"), $(".full img").addClass("fullsize")) : $(".full img").removeClass("fullsize")
}

function detectMargin() {
    var e = $(".full").children().width(),
        t = $(".full").children().height(),
        i = $(window).height(),
        a = $(window).width();
    a > e ? $(".full").children().css({
        "margin-left": a / 2 - e / 2
    }) : $(".full").children().css({
        "margin-left": 0
    }), i > t ? $(".full").children().css({
        "margin-top": i / 2 - t / 2
    }) : $(".full").children().css({
        "margin-top": 0
    })
}

function LocationHash() {
    var e = window.location.hash;
    e = e.slice(1), Arrhash = e.split("/"), $(".link-page a[data-name='" + e + "']").trigger("click"), $("#progress-page").length ? 1 == isLoad && (Arrhash.length > 1 ? ($(".sub-nav li a[data-name='" + Arrhash[0] + "']").trigger("click"), monthHash = e) : $(".sub-nav li a[data-name='" + e + "']").trigger("click")) : $(".sub-nav li a[data-name='" + e + "']").trigger("click"), $(".sub-nav-block a[data-name='" + e + "']").trigger("click"), $(".sub-nav-typical li a[data-name='" + e + "']").trigger("click")
}! function(e) {
    var t = {
        on: e.fn.on,
        bind: e.fn.bind
    };
    e.each(t, function(i) {
        e.fn[i] = function() {
            var e, a = [].slice.call(arguments),
                o = a.pop(),
                l = a.pop();
            return a.push(function() {
                var t = this,
                    i = arguments;
                clearTimeout(e), e = setTimeout(function() {
                    l.apply(t, [].slice.call(i))
                }, o)
            }), t[i].apply(this, isNaN(o) ? arguments : a)
        }
    })
}(jQuery), document.addEventListener("keyup", doc_keyUp, !1),
    function(e) {
        e.fn.clipPath = function() {
            return this.filter("[data-clipPath]").each(function(t) {
                var i = e(this).attr("data-clipPath"),
                    a = e(this).attr("src"),
                    o = e(this).css("width"),
                    l = e(this).css("height"),
                    n = parseFloat(o, 10),
                    s = parseFloat(l, 10),
                    r = e(this).attr("alt"),
                    c = e(this).attr("title"),
                    d = t,
                    h = e('<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgMask" width="' + o + '" height="' + l + '" viewBox="0 0 ' + n + " " + s + '"><defs><clipPath id="maskID' + d + '"><path d="' + i + '"/></clipPath>  </defs><title>' + c + "</title><desc>" + r + '</desc><image clip-path="url(#maskID' + d + ')" width="' + o + '" height="' + l + '" xlink:href="' + a + '" /></svg>');
                e(this).replaceWith(h), delete i, h
            }), this
        }
    }(jQuery);
var timex, Click, News = 0,
    shownews, show, Menu = 0,
    SubMenu = 0,
    Details = 0,
    doWheel = !0,
    doTouch = !0,
    Has = !0,
    Arrhash, monthHash = null,
    date = new Date,
    curMonth = date.getMonth(),
    curYear = date.getFullYear(),
    timer, isLoad = 1;
$(document).ready(function() {
    $(document).bind("scroll", function() {
        var e = $(document).scrollTop(),
            t = $(".mobile-bg img").height();
        if ($(window).width() <= 1100 && (e > 50 ? $(".scroll-down").fadeOut(500, "linear") : $(".scroll-down").fadeIn(500, "linear"), e > $(window).height() / 2 ? $(".go-top").css({
                display: "block",
                opacity: 1
            }) : $(".go-top").css({
                display: "none",
                opacity: 0
            }), $(".popup-pics-mobile img").length > 0)) {
            $(".popup-pics-mobile").offset().top;
            e > t / 2 ? RenderImage() : ($(".popup-pics-mobile img").removeClass("fadein"), $(".popup-pics-mobile").css({
                height: 0,
                padding: 0
            }))
        }
    }), $(".navigation").bind("scroll", function() {
        var e = $(".navigation").scrollTop();
        e > 40 ? $(".right").addClass("hide-right") : $(".right").removeClass("hide-right")
    }), $(".go-top").click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, "slow")
    }), $(".container").click(function() {
        $(".top, .overlay-menu, .right").removeClass("show"), $(".nav-click").removeClass("active"), $("html, body").removeClass("no-scroll")
    }), $(".overlay-menu, .navigation span, .nav,  .right").click(function() {
        $(window).width() <= 1100 && ($(".top, .overlay-menu, .right").removeClass("show"), $(".nav-click").removeClass("active"), $("html, body").removeClass("no-scroll"))
    })
}), window.onorientationchange = ResizeWindows, $(window).on("orientationchange", function() {
    $(window).width() <= 1100 && (ScrollHoz(), $("#news-page").length && detectBut(), $(".sub-nav-block li").hasClass("current") ? $(".sub-nav-block li.current").trigger("click") : $(".sub-nav-block li:first-child").trigger("click"), $(".sub-nav-typical li").hasClass("current") ? $(".sub-nav-typical li.current ").trigger("click") : $(".sub-nav-typical li:first-child ").trigger("click"), $(".zoom-pic").length && $(window).width() > 740 && ($("html, body").removeClass("no-scroll"), $(".close-pics-small").trigger("click")))
}), $(window).resize(function() {
    ScrollNiceHide(), ResizeWindows()
}), $(window).on("resize", function() {
    if (ResizeWindows(), $(window).width() > 1100) {
        if (Calculation(), $(".nav-click").hasClass("active") && $(".container").trigger("click"), $(".dragscroll").length && (detectMargin(), $(".dragscroll").draptouch()), $(".viewer").hasClass("desktop") || ZoomMap(), $(".contact-box .column").hasClass("fadeinup") || $(".contact-box").children().each(function(e) {
                var t = $(this);
                setTimeout(function() {
                    $(t).addClass("fadeinup")
                }, 500 * (e + 1))
            }), $(".sub-nav-block li").hasClass("current") ? $(".sub-nav-block li.current").trigger("click") : $(".sub-nav-block li:first-child").trigger("click"), $("#about-page").length) {
            if ($(".item-active").length) {
                var e = $(".slide-bg")[0].swiper;
                e.onResize()
            } else SlidePicture();
            $(".sub-nav li").hasClass("current") ? $(".sub-nav li.current ").trigger("click") : $(".sub-nav li:first-child ").trigger("click")
        }
        $("#news-page, #office-page").length && ($(".colum-box-news").hasClass("fadeinup") || ($(".news-list").addClass("goleft"), $(".colum-box-news").addClass("fadeinup"))), $("#location-page").length && ($(".viewer").css({
            display: "block"
        }), $(".map-mobile").css({
            display: "none"
        })), $("#apartment-details-page").length && ($(".sub-nav-typical li.current ").trigger("click"), setTimeout(function() {
            $(".sub-nav-typical li.current ").trigger("click")
        }, 850)), $("#library-page").length && ($(".video-active").length || $(".video-slide").BTQSlider({
            singleItem: !0,
            slideSpeed: 600,
            paginationSpeed: 600,
            navigation: !0,
            pagination: !0,
            afterAction: function() {
                this.$BTQItems.removeClass("video-active"), this.$BTQItems.eq(this.currentItem).addClass("video-active")
            }
        }), $(".brochure-active").length || $(".brochure-slide").BTQSlider({
            singleItem: !0,
            slideSpeed: 600,
            paginationSpeed: 600,
            navigation: !0,
            pagination: !0,
            afterAction: function() {
                this.$BTQItems.removeClass("brochure-active"), this.$BTQItems.eq(this.currentItem).addClass("brochure-active")
            }
        })), $(".zoom-pic").length && ($(".project-load").length || ($("html, body").removeClass("no-scroll"), $(".close-pics-small").trigger("click"))), $(".scrollA, .scrollB, .scrollC, .scroll-des, .scroll-menu,  .scroll-pro").length && ($(".scrollB").css({
            width: "100%"
        }), setTimeout(function() {
            ScrollNiceA(), ScrollNiceB(), ScrollNiceC(), ScrollMenu(), ScrollDes(), ScrollPro()
        }, 150))
    } else {
        if (detectHeight(), 0 != TouchLenght && isTouchDevice || (Calculation(), ScrollHoz(), ($("#news-page").length || $("#progress-page").length) && detectBut(), $(".sub-nav-block li").hasClass("current") ? $(".sub-nav-block li.current").trigger("click") : $(".sub-nav-block li:first-child").trigger("click"), $(".sub-nav-typical li").hasClass("current") ? $(".sub-nav-typical li.current ").trigger("click") : $(".sub-nav-typical li:first-child ").trigger("click"), $(".zoom-pic").length && $(window).width() > 740 && ($(".project-load").length || ($("html, body").removeClass("no-scroll"), $(".close-pics-small").trigger("click")))), $("#location-page").length && ($(".viewer").css({
                display: "none"
            }), $(".map-mobile").css({
                display: "block"
            })), $("#about-page").length) {
            if ($(".item-active").length) {
                var e = $(".slide-bg")[0].swiper;
                e.destroy(!0, !0)
            }
            $(".slide-item.selected").length && $(".projects").data("BTQSlider").destroy()
        }
        $("#library-page").length && ($(".video-active").length && $(".video-slide").data("BTQSlider").destroy(), $(".brochure-active").length && $(".brochure-slide").data("BTQSlider").destroy()), $(".dragscroll").length && (detectMargin(), $(".dragscroll").draptouch())
    }
    $("#library-page").length && ($(".video-list").length || $(".library-center").trigger("BTQ.prev.htm"))
}, 250), $(window).bind("popstate", function(e) {
    $(window).width() > 1100 && (e.preventDefault(), LinkPage());
    var t = $(".httpserver").text();
    if ($(window).width() > 1100)
        if (null !== e.originalEvent.state) {
            var i = e.originalEvent.state.path,
                a = e.originalEvent.state.dataName,
                o = e.originalEvent.state.title,
                l = document.URL;
            changeUrl(i, o, "", "", a, "", "");
            var n = i.replace(t, ""),
                s = n.split("/");
            $(".nav li a").each(function(e, t) {
                $(t).attr("href") == i && window.history.back()
            }), $("#about-page").length && ($(".close-content").length && $(".close-content").trigger("click"), $(".sub-nav li").each(function(e, t) {
                $(t).find("a").attr("href") == i && $(t).trigger("click")
            }), $(".link-project").each(function(e, t) {
                $(t).attr("href") == i && $(t).trigger("click")
            })), $("#block-page").length && ($(".link-page-active").attr("href") == i && window.history.back(), $(".sub-nav-block li").each(function(e, t) {
                $(t).find("a").attr("href") == i && $(t).trigger("click")
            })), $("#apartment-details-page").length && $(".sub-nav-typical li").each(function(e, t) {
                $(t).find("a").attr("href") == i && $(t).trigger("click")
            }), $("#news-page").length && $(".link-page a").each(function(e, t) {
                $(t).attr("href") == i && $(t).trigger("click")
            }), $("#progress-page").length && ("" != s[2] && void 0 != s[2] ? $(".sub-nav li.current a").attr("href") != t + s[0] + "/" + s[1] + ".html" ? $(".sub-nav li a[href='" + t + s[0] + "/" + s[1] + ".html']").parent().trigger("click") : $(".progress-month li a[href='" + i + "']").trigger("click") : "" != s[1] && void 0 != s[1] && ($(".sub-nav li.current a").attr("href") != t + s[0] + "/" + s[1] + ".html" ? window.history.back() : $(".sub-nav li a[href='" + t + s[0] + "/" + s[1] + ".html']").parent().trigger("click")))
        } else {
            var l = document.URL,
                n = l.replace(t, ""),
                s = n.split("/");
            $(".nav li a").each(function(e, t) {
                $(t).attr("href") == l && window.history.back()
            }), $("#about-page").length && ($(".close-content").length && $(".close-content").trigger("click"), $(".sub-nav li").each(function(e, t) {
                $(t).find("a").attr("href") == l && $(t).trigger("click")
            }), $(".link-project").each(function(e, t) {
                $(t).attr("href") == l && $(t).trigger("click")
            })), $("#block-page").length && ($(".link-page-active").attr("href") == l && window.history.back(), $(".sub-nav-block li").each(function(e, t) {
                $(t).find("a").attr("href") == l && $(t).trigger("click")
            })), $("#apartment-details-page").length && $(".sub-nav-typical li").each(function(e, t) {
                $(t).find("a").attr("href") == l && $(t).trigger("click")
            }), $("#news-page").length && $(".link-page a").each(function(e, t) {
                $(t).attr("href") == l && $(t).trigger("click")
            }), $("#progress-page").length && ("" != s[2] && void 0 != s[2] ? $(".sub-nav li.current a").attr("href") != t + s[0] + "/" + s[1] + ".html" ? $(".sub-nav li a[href='" + t + s[0] + "/" + s[1] + ".html']").parent().trigger("click") : $(".progress-month li a[href='" + l + "']").trigger("click") : "" != s[1] && void 0 != s[1] && ($(".sub-nav li.current a").attr("href") != t + s[0] + "/" + s[1] + ".html" ? window.history.back() : $(".sub-nav li a[href='" + t + s[0] + "/" + s[1] + ".html']").parent().trigger("click")))
        } else {
        if (null !== e.originalEvent.state) var i = e.originalEvent.state.path;
        else var i = document.URL;
        var n = i.replace(t, ""),
            s = n.split("/");
        $(".nav li a").each(function(e, t) {
            i == $(t).attr("href") && window.history.back()
        }), $("#about-page").length && ($(".close-content").length && $(".close-content").trigger("click"), $(".link-project").each(function(e, t) {
            $(t).attr("href") == i && $(t).trigger("click")
        })), $("#block-page").length && ($(".link-page-active").attr("href") == i ? window.history.back() : location.reload()), $("#apartment-details-page").length && location.reload(), $("#progress-page").length && ("" != s[2] && void 0 != s[2] ? $(".sub-nav li.current a").attr("href") != t + s[0] + "/" + s[1] + ".html" ? $(".sub-nav li a[href='" + t + s[0] + "/" + s[1] + ".html']").parent().trigger("click") : $(".progress-month li a[href='" + i + "']").trigger("click") : "" != s[1] && void 0 != s[1] && ($(".sub-nav li.current a").attr("href") != t + s[0] + "/" + s[1] + ".html" ? window.history.back() : $(".sub-nav li a[href='" + t + s[0] + "/" + s[1] + ".html']").parent().trigger("click"))), $("#news-page").length && location.reload()
    }
});
