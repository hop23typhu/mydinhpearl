<?php //template name: Liên hệ ?>
<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/contact/bg.jpg)"><h1>Liên hệ</h1></div>
<div class="content-page">
<div class="bg-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg6.jpg)"></div>  
<div class="contact-content">
<div class="contact-box">
<div class="column">
<div class="contact-text">
<h3>Phòng kinh doanh dự án Mỹ Đình Pearl</h3>
<ul>
<li class="address">Số 01 Châu Văn Liêm, Phường Phú Đô, Quận Nam Từ Liêm, Hà Nội</li>

<li class="cellphone"><a href="tel:<?php echo str_replace(' ', '', print_option('hotline'));  ?>"><?php echo print_option('hotline'); ?></a></li>
<li class="cellphone"><a href="tel:<?php echo str_replace(' ', '', print_option('hotline2'));  ?>"><?php echo print_option('hotline2'); ?></a></li>
<li class="email"><a href="mailto:<?php echo print_option('email'); ?>"><?php echo print_option('email'); ?></a></li>
</ul>
<p>Quý khách hàng vui lòng nhập thông tin liên hệ, chúng tôi sẽ liên hệ với Quý khách hàng trong thời gian sớm nhất!<br />
Trân trọng cảm ơn!</p></div>
</div>
<div class="column">
<div class="contact-form">
<form onSubmit="return validatecontact();" id="contact" name="frm_contact"  method="post">
<div class="require-col">
<div class="input-text">
<input type="text" data-holder="Họ và Tên (*)" value="Họ và Tên (*)" id="name" name="name" >
</div>
<div class="input-text ">
<input type="text" data-holder="Địa chỉ" value="Địa chỉ" id="address" name="address" >
</div>
<div class="input-text ">
<input type="text" data-holder="Điện thoại (*)" value="Điện thoại (*)" id="phone" name="phone" >
</div>
<div class="input-text">
<input type="text" data-holder="Email (*)"  value="Email (*)" id="email" name="email" >
</div>
<div class="input-area">
<textarea data-holder="Nội dung (*)" id="comments" name="comments" >Nội dung (*)</textarea>
</div>  
<div class="input-but">
<input id="btn-contact-reset" type="reset" value="Xóa" >
<input id="btn-contact-submit" type="button" value="Gửi" >
<input  type="hidden" name="action" value="faci_contact" >
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
<script>
function validatecontact(){
	hidemsg();
	var flag = true;
	var name = checkNull('name', "Vui lòng nhập họ tên!","Họ và Tên (*)",'40','-298');
	var comments = checkNull('comments', "Vui lòng nhập nội dung!","Nội dung (*)",'40','-298');
	var phone = checkNull('phone', "Điện thoại không hợp lệ!","Điện thoại (*)",'40','-298');
	var email = checkMail('email', "Email không hợp lệ!","Email (*)",'40','-298');	
	if(!name || !comments || !phone || !email ){
		flag = false;
		setTimeout(hideerror,5000);
	}
	return flag;
}
$('#btn-contact-submit').click(function(){ 
	if(validatecontact()==true){
		$("#contact-loader").show();
		var datapost = $('#contact').serialize(); 
		$.ajax({  
		  type: "POST",  
		  url: "<?php echo admin_url('admin-ajax.php'); ?>",
		  data: datapost,
		  success: function(success) { 
			$("#contact-loader").hide();
			$('.contact-success').remove();
			if(success=='yes'){
				$('#btn-contact-reset').trigger('click');
				$('#contact-loader').after("<div  class='contact-success color-blue'>Thông tin liên hệ của Bạn được gửi hoàn tất! Xin cảm ơn!</div>");							
			}else{
				$('#contact-loader').after("<div  class='contact-success color-red'>Thông tin liên hệ của bạn không gửi được, vui lòng kiểm tra lại!</div>");
			}
			$('html, body').animate({scrollTop:0}, 'slow');
			setTimeout(hidemsg,5000);
		  }  
		});  //end ajax 
		return false; 
	}//end if
	$(".formError").click(function(){
		$(this).hide();	
	});	
	return false;			
});
$('#btn-contact-reset').click(function(){
	hideerror();
});
$("#phone").numeric();
$('#contact').keydown(function(e){
	if(!$("textarea").is(":focus")){
	if (e.keyCode == 13) {
		$('#btn-contact-submit').trigger('click');
	}
	}
});
</script>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>