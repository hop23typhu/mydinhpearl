<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<!--DESKTOP INTRO--> 
<div class="slider-home"  data-time="6000"> 
<div class="slide-bg">
<div class="pagination"></div>
<div class="item-wrapper">
<div class="item-container">
<div class="bg-home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg1.jpg)"></div>
</div>
<div class="item-container">
<div class="bg-home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg2.jpg)"></div>
</div>
<div class="item-container">
<div class="bg-home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg3.jpg)"></div>
</div>
<div class="item-container">
<div class="bg-home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg4.jpg)"></div>
</div>
<div class="item-container">
<div class="bg-home" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg5.jpg)"></div>
</div>
</div>
</div>
</div>
<!--DESKTOP INTRO--> 
<!--CONTENT-MOBILE--> 
<section id="location">
<div class="location-mobile">
<h3>Ngọc xanh trong lòng Hà Nội</h3><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/location/map_s.jpg"  alt="Ngọc xanh trong lòng Hà Nội">
<a class="zoom-mobile" href="javascript:void(0);" data-src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/location/map_l.jpg" >Phóng to ảnh</a>
</div>
</section>
<section id="facilities">
<div class="mobile-intro">
<div class="text-intro">
<p>Với ý tưởng tạo ra một tổ ấm đích thực, an lành cho cư dân tại Mỹ Đình Pearl, chúng tôi cùng đơn vị thiết kế đã chăm chút từng chi tiết để tạo nên tổ hợp kiến trúc hiện đại theo tiêu chuẩn quốc tế ...</p></div>
<img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/full-mobile.jpg"  alt="Ngọc xanh trong lòng Hà Nội">
<a class="go-page" href="thong-tin-can-ho.html" >SƠ ĐỒ TỔNG THỂ</a></div>
</section>
<!--CONTENT-MOBILE-->
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>