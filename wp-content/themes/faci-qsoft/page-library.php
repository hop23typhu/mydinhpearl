<?php //template name:Thư viện ?>
<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<a  href="javascript:void(0);" class="show-content"><span>quay lại</span></a>
<a  href="javascript:void(0);" class="hide-content"><span>xem toàn màn hình</span></a>

<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/library/bg.jpg)"><h1>Thư viện</h1></div>
<div class="content-page">
<div class="library-album"></div>
<div class="sub-library">
<div class="thumb-list">
<h2>Hình ảnh</h2>
<div class="thumb-slide">
<div class="item-thumb"><a  class="link-pic" href="javascript:void(0);" data-href="<?php bloginfo('wpurl'); ?>/thu-vien/album-noi-that" ><h3>Nội Thất</h3><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/library/1.jpg" alt="Nội Thất"></a></div>
<div class="item-thumb"><a  class="link-pic" href="javascript:void(0);" data-href="<?php bloginfo('wpurl'); ?>/thu-vien/album-phoi-canh" ><h3>Phối Cảnh</h3><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/library/2.jpg" alt="Phối Cảnh"></a></div>
<div class="item-thumb"><a  class="link-pic" href="javascript:void(0);" data-href="<?php bloginfo('wpurl'); ?>/thu-vien/album-tien-ich" ><h3>Tiện ích</h3><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/library/3.jpg" alt="Tiện ích"></a></div>
</div>
</div>
<div class="video-box">
<h2>Video</h2>
<div class="video-slide">
<div class="item-thumb"><a class="player" href="javascript:void(0);" data-href="<?php bloginfo('wpurl'); ?>/video" ></a><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/video/2.jpg" alt="Dự án Mỹ Đình Pearl"></div>
</div>
</div>
</div>
</div>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>