<?php
/**
 * @package WordPress default theme
 * @subpackage Deeds theme of qsoft
 * @since deeds 1.0.3 (04/07/2016)
 * @author   Luong Ba Hop
 */
add_action( 'after_setup_theme', 'deeds_start_setup' );
if ( ! function_exists( 'deeds_start_setup' ) ) {
	function deeds_start_setup() {
		global $themename, $shortname, $options, $theme_path, $theme_uri;
		$themename  = 'deeds';
		$shortname  = 'deeds';
		load_theme_textdomain( 'deeds', get_template_directory() . '/languages' );
		$theme_path = get_template_directory();
		$theme_uri  = get_template_directory_uri();
		require_once( $theme_path . '/includes/install_plugins.php' );//install plugins theme need
		require_once( $theme_path . '/d_options/d-display.php' );//display option 
		require_once( $theme_path . '/d_options/d-custom.php' );
		//require_once( $theme_path . '/d_options/inc/mailchimp/d-mailchimp.php' );
		require_once( $theme_path . '/includes/functions_mail.php' );//process about mail
		require_once( $theme_path . '/includes/functions_frontend.php' );
		require_once( $theme_path . '/includes/functions_init.php' );
		require_once( $theme_path . '/includes/functions_styles.php' );//print option  Typography on head tag
		require_once( $theme_path . '/includes/functions_shortcodes.php' );// make shortcode for website . All shortcode here
		require_once( $theme_path . '/includes/function_ajax.php' );//process ajax 
		require_once( $theme_path . '/includes/custom-header.php' );
		require_once( $theme_path . '/includes/template-tags.php' );
		require_once( $theme_path . '/includes/extras.php' );
		require_once( $theme_path . '/includes/jetpack.php' );
		/** Create navigation **/
		if ( function_exists( 'wp_nav_menu') ) {
			add_theme_support( 'nav-menus' );
			register_nav_menus( array( 'primary-menu' => __( 'Primary Menu', $themename ) ) );
			register_nav_menus( array( 'cat-menu' => __( 'Sidebar Menu', $themename ) ) );
			register_nav_menus( array( 'bot-menu' => __( 'Bottom Menu', $themename ) ) );
		}
		/** Create sidebar **/
		register_sidebar( array(
			'name' => __( 'Sidebar widgets', $themename ),
			'id' => 'sidebar-widgets',
			'description' => __( 'Sidebar widgets', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer widgets', $themename ),
			'id' => 'footer-widgets',
			'description' => __( 'Footer widgets', $themename ),
			'before_widget' => '<div  class="text">',
			'after_widget' => '</div>',
			'before_title' => '<h4>',
			'after_title' => '</h4>',
		) );
		register_sidebar( array(
			'name' => __( 'Footer partner', $themename ),
			'id' => 'footer-partner',
			'description' => __( 'Footer partner', $themename ),
			'before_widget' => ' ',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		register_sidebar( array(
			'name' => __( 'Footer box ', $themename ),
			'id' => 'footer-2box',
			'description' => __( 'Footer box', $themename ),
			'before_widget' => '',
			'after_widget' => '',
			'before_title' => '',
			'after_title' => '',
		) );
		//add_theme_support( 'custom-logo' );//support LOGO
		add_theme_support( 'automatic-feed-links' );//make feed link in head
		add_theme_support( 'title-tag' );//make auto title tag
		add_theme_support( 'html5', array(//support html5 tags 
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'post-formats', array(//support some post format in post
			'video',
		) );
		add_theme_support( 'post-thumbnails' );//ddd post thumbnail
		//custom your template
		add_theme_support( 'custom-background', apply_filters( 'deeds_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
	}
}
//unregister some widgets
function remove_default_widgets() {
     unregister_widget('WP_Widget_Pages');
     unregister_widget('WP_Widget_Calendar');
     unregister_widget('WP_Widget_Archives');
     unregister_widget('WP_Widget_Links');
     unregister_widget('WP_Widget_Meta');
     unregister_widget('WP_Widget_Search');
     unregister_widget('WP_Widget_Text');
     unregister_widget('WP_Widget_Recent_Posts');
     unregister_widget('WP_Widget_Recent_Comments');
     unregister_widget('WP_Widget_RSS');
}
add_action('widgets_init', 'remove_default_widgets', 11);
//Prevent hack SQL Injection 
global $user_ID; 
if($user_ID) {
	if(!current_user_can('administrator')) {
        if (strlen($_SERVER['REQUEST_URI']) > 255 ||
            stripos($_SERVER['REQUEST_URI'], "eval(") ||
            stripos($_SERVER['REQUEST_URI'], "CONCAT") ||
            stripos($_SERVER['REQUEST_URI'], "UNION+SELECT") ||
            stripos($_SERVER['REQUEST_URI'], "base64"))
            {
                    @header("HTTP/1.1 414 Request-URI Too Long");
                    @header("Status: 414 Request-URI Too Long");
                    @header("Connection: Close");
                    @exit;
    		}
	}
}
//remove menubar (top) with user
if ( !current_user_can( 'manage_options' ) ) {
	add_filter( 'show_admin_bar', '__return_false' );
}
//return limit 30 word for excerpt
function custom_exerpt_length($length){
	return 30;
}
add_filter('excerpt_length','custom_exerpt_length',999);
//change excerpt read more
function new_excerpt_more( $more ) {
	return '...';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );
//info about me
function faci_dashboard() { ?>
	<h3>Website <?php echo bloginfo( 'name' ); ?>.</h3>
	<p><strong>Info contact of author</strong><br>
	<strong>Web Developer</strong> :  <a target="_blank" href="https://www.facebook.com/steve.luong.5">Luong Ba Hop</a><br>
	<strong>Email</strong>  luonghop.lc@gmail.com<br>
	<strong>Cellphone</strong> : 01632.434.165<br>
	<strong>Website</strong> : <a target="_blank" href="http://teachyourself.vn/">Teachyourself.vn</a></p> 
<?php }
add_action('wp_dashboard_setup', 'faci_welcome');
function faci_welcome() {
	global $wp_meta_boxes;
	wp_add_dashboard_widget('custom_help_widget', 'About me', 'faci_dashboard');
}
//change footer backend wordpress
add_filter('admin_footer_text', 'remove_footer_admin');
function remove_footer_admin () {
	echo 'Powered by Faci Theme | Designed by <a href="http://teachyourself.vn">Luong Ba Hop</a> </p>';
}
// disable logo wordpress in backend
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
}
// add target blank to visit site
add_action( 'admin_bar_menu', 'customize_my_wp_admin_bar', 80 );
function customize_my_wp_admin_bar( $wp_admin_bar ) {
    //Get a reference to the view-site node to modify.
    $node = $wp_admin_bar->get_node('view-site');
    //Change target
    $node->meta['target'] = '_blank';
    //Update Node.
    $wp_admin_bar->add_node($node);
}
function set_id_page()
{
	$_id='home-page';
	if(is_home() || is_front_page()) $_id='home-page';
	elseif(is_page_template( 'page-contact.php' )) $_id='contact-page';
	elseif( 
		is_page_template( 'page-about.php' ) ||
		is_page_template( 'gioi-thieu/gioi-thieu-du-an.php' ) ||
		is_page_template( 'gioi-thieu/chu-dau-tu.php' ) ||
		is_page_template( 'gioi-thieu/doi-tac.php' ) ||
		is_page_template( 'gioi-thieu/quy-mo-du-an.php' ) ||
		is_page_template( 'gioi-thieu/y-tuong-thiet-ke.php' ) 
	) $_id='about-page';
	elseif(is_page_template( 'page-location.php' )) $_id='location-page';
	elseif(is_page_template( 'page-facilities.php' )) $_id='facilities-page';
	elseif(
		is_page_template( 'page-news.php' ) ||
		is_single()
	) $_id='news-page';
	elseif(is_page_template( 'page-library.php' )) $_id='library-page';
	elseif(is_page_template( 'page-apartment.php' )) $_id='apartment-page';
	elseif(
		is_page_template( 'thong-tin-can-ho/page-pearl-01.php' ) ||
		is_page_template( 'thong-tin-can-ho/page-pearl-02.php' )
	) $_id='block-page';

	return $_id;
}
/*
	**************END***************
*/