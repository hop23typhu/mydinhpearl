<?php //template name: Pearl 02 ?>
<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<div class="title-page" style="background-image:url(<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/bg3.jpg)"><h1>Thông tin căn hộ</h1></div>
<div class="content-page">
<div class="sub-nav-block">
<ul>
<li  ><h3>Tầng 03 - 32</h3><a href="pearl-02/tang-03---32.html"  data-name="f1" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="0"></a></li>
<li  ><h3>Tầng 33</h3><a href="pearl-02/tang-33.html"  data-name="f2" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="1"></a></li>
<li  ><h3>Tầng 34</h3><a href="pearl-02/tang-34.html"  data-name="f3" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="2"></a></li>
<li  ><h3>Tầng 35</h3><a href="pearl-02/tang-35.html"  data-name="f4" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="3"></a></li>
<li  ><h3>Tầng 36</h3><a href="pearl-02/tang-36.html"  data-name="f5" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="4"></a></li>
<li  ><h3>Tầng 37</h3><a href="pearl-02/tang-37.html"  data-name="f6" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="5"></a></li>
<li  ><h3>Tầng 38</h3><a href="pearl-02/tang-38.html"  data-name="f7" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" data-open="6"></a></li>
</ul>
</div>
<div class="title-block"><h2>Tháp PEARL 02 / </h2><h3>floor</h3></div>
<div class="box-content">
<!--tang 3-32-->
<div class="colum-box" id="0">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-03-32-1" href="pearl-02/tang-03---32/can-so-01.html"  data-info="2"><div class="num-block">P2-01</div></a>
<a class="thumb-03-32-2" href="pearl-02/tang-03---32/can-ho-02.html"  data-info="3"><div class="num-block">P2-02</div></a>
<a class="thumb-03-32-3" href="pearl-02/tang-03---32/can-ho-03.html"  data-info="5"><div class="num-block">P2-03</div></a>
<a class="thumb-03-32-4" href="pearl-02/tang-03---32/can-ho-04.html"  data-info="1"><div class="num-block">P2-04</div></a>
<a class="thumb-03-32-5" href="pearl-02/tang-03---32/can-ho-05.html"  data-info="4"><div class="num-block">P2-05</div></a>
<a class="thumb-03-32-6" href="pearl-02/tang-03---32/can-ho-06.html"  data-info="6"><div class="num-block">P2-06</div></a>
<a class="thumb-03-32-7" href="pearl-02/tang-03---32/can-ho-07.html"  data-info="7"><div class="num-block">P2-07</div></a>
<a class="thumb-03-32-8" href="pearl-02/tang-03---32/can-ho-08.html"  data-info="8"><div class="num-block">P2-08</div></a>
<a class="thumb-03-32-9" href="pearl-02/tang-03---32/can-ho-09.html"  data-info="9"><div class="num-block">P2-09</div></a>
<a class="thumb-03-32-10" href="pearl-02/tang-03---32/can-ho-10.html"  data-info="10"><div class="num-block">P2-10</div></a>
<a class="thumb-03-32-11" href="pearl-02/tang-03---32/can-ho-11.html"  data-info="11"><div class="num-block">P2-11</div></a>
<a class="thumb-03-32-12" href="pearl-02/tang-03---32/can-ho-12.html"  data-info="12"><div class="num-block">P2-12</div></a>
<a class="thumb-03-32-12a" href="pearl-02/tang-03---32/can-ho-12a.html"  data-info="13"><div class="num-block">P2-12A</div></a>
<a class="thumb-03-32-14" href="pearl-02/tang-03---32/can-ho-14.html"  data-info="14"><div class="num-block">P2-14</div></a>
<a class="thumb-03-32-15" href="pearl-02/tang-03---32/can-ho-15.html"  data-info="15"><div class="num-block">P2-15</div></a>
</div>
</div>
<div class="block-bg block3-32"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="2"><a  href="pearl-02/tang-03---32/can-so-01.html" ><h3><strong>P2-01 (A1)</strong> [54.27 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="3"><a  href="pearl-02/tang-03---32/can-ho-02.html" ><h3><strong>P2-02 (C1)</strong> [90.42 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="5"><a  href="pearl-02/tang-03---32/can-ho-03.html" ><h3><strong>P2-03 (C1)</strong> [90.45 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="1"><a  href="pearl-02/tang-03---32/can-ho-04.html" ><h3><strong>P2-04 (A1)</strong> [53.28 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="4"><a  href="pearl-02/tang-03---32/can-ho-05.html" ><h3><strong>P2-05 (B2)</strong> [83.99 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="6"><a  href="pearl-02/tang-03---32/can-ho-06.html" ><h3><strong>P2-06 (C2A)</strong> [104.13 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="7"><a  href="pearl-02/tang-03---32/can-ho-07.html" ><h3><strong>P2-07 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="8"><a  href="pearl-02/tang-03---32/can-ho-08.html" ><h3><strong>P2-08 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="9"><a  href="pearl-02/tang-03---32/can-ho-09.html" ><h3><strong>P2-09 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="10"><a  href="pearl-02/tang-03---32/can-ho-10.html" ><h3><strong>P2-10 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="11"><a  href="pearl-02/tang-03---32/can-ho-11.html" ><h3><strong>P2-11 (C2B)</strong> [104.74 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="12"><a  href="pearl-02/tang-03---32/can-ho-12.html" ><h3><strong>P2-12 (C2B)</strong> [104.74 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="13"><a  href="pearl-02/tang-03---32/can-ho-12a.html" ><h3><strong>P2-12A (B1)</strong> [79.84 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="14"><a  href="pearl-02/tang-03---32/can-ho-14.html" ><h3><strong>P2-14 (A2)</strong> [54.12 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="15"><a  href="pearl-02/tang-03---32/can-ho-15.html" ><h3><strong>P2-15 (A2)</strong> [55.77 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
<!--tang 33-->
<div class="colum-box" id="1">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-33-1" href="pearl-02/tang-33/can-so-01-1.html"  data-info="16"><div class="num-block">P2-01</div></a>
<a class="thumb-33-2" href="pearl-02/tang-33/can-ho-02-1.html"  data-info="17"><div class="num-block">P2-02</div></a>
<a class="thumb-33-3" href="pearl-02/tang-33/can-ho-03-1.html"  data-info="18"><div class="num-block">P2-03</div></a>
<a class="thumb-33-4" href="pearl-02/tang-33/can-ho-04-1.html"  data-info="19"><div class="num-block">P2-04</div></a>
<a class="thumb-33-5" href="pearl-02/tang-33/can-ho-05-1.html"  data-info="20"><div class="num-block">P2-05</div></a>
<a class="thumb-33-6" href="pearl-02/tang-33/can-ho-06-1.html"  data-info="21"><div class="num-block">P2-06</div></a>
<a class="thumb-33-7" href="pearl-02/tang-33/can-ho-07-1.html"  data-info="22"><div class="num-block">P2-07</div></a>
<a class="thumb-33-8" href="pearl-02/tang-33/can-ho-08-1.html"  data-info="23"><div class="num-block">P2-08</div></a>
<a class="thumb-33-9" href="pearl-02/tang-33/can-ho-09-1.html"  data-info="24"><div class="num-block">P2-09</div></a>
<a class="thumb-33-10" href="pearl-02/tang-33/can-ho-10-1.html"  data-info="25"><div class="num-block">P2-10</div></a>
<a class="thumb-33-11" href="pearl-02/tang-33/can-ho-11-1.html"  data-info="26"><div class="num-block">P2-11</div></a>
<a class="thumb-33-12" href="pearl-02/tang-33/can-ho-12-1.html"  data-info="27"><div class="num-block">P2-12</div></a>
</div>
</div>
<div class="block-bg block33"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="16"><a  href="pearl-02/tang-33/can-so-01-1.html" ><h3><strong>P2-01 (A1)</strong> [54.3 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="17"><a  href="pearl-02/tang-33/can-ho-02-1.html" ><h3><strong>P2-02 (C1)</strong> [90.41 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="18"><a  href="pearl-02/tang-33/can-ho-03-1.html" ><h3><strong>P2-03 (C1)</strong> [90.41 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="19"><a  href="pearl-02/tang-33/can-ho-04-1.html" ><h3><strong>P2-04 (A1)</strong> [53.28 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="20"><a  href="pearl-02/tang-33/can-ho-05-1.html" ><h3><strong>P2-05 (B2)</strong> [84.03 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="21"><a  href="pearl-02/tang-33/can-ho-06-1.html" ><h3><strong>P2-06 (C2A)</strong> [104.14 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="22"><a  href="pearl-02/tang-33/can-ho-07-1.html" ><h3><strong>P2-07 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="23"><a  href="pearl-02/tang-33/can-ho-08-1.html" ><h3><strong>P2-08 (B1)</strong> [78.99 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="24"><a  href="pearl-02/tang-33/can-ho-09-1.html" ><h3><strong>P2-09 (B1)</strong> [79.02 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="25"><a  href="pearl-02/tang-33/can-ho-10-1.html" ><h3><strong>P2-10 (D1)</strong> [286.22 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="26"><a  href="pearl-02/tang-33/can-ho-11-1.html" ><h3><strong>P2-11 (D1)</strong> [286.23 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="27"><a  href="pearl-02/tang-33/can-ho-12-1.html" ><h3><strong>P2-12 (C3)</strong> [109.73 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
<!--tang 34-->
<div class="colum-box" id="2">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-34-1" href="pearl-02/tang-34/can-ho-01.html"  data-info="28"><div class="num-block">P2-01</div></a>
<a class="thumb-34-2" href="pearl-02/tang-34/can-ho-02-2.html"  data-info="29"><div class="num-block">P2-02</div></a>
<a class="thumb-34-3" href="pearl-02/tang-34/can-ho-03-2.html"  data-info="30"><div class="num-block">P2-03</div></a>
<a class="thumb-34-4" href="pearl-02/tang-34/can-ho-04-2.html"  data-info="31"><div class="num-block">P2-04</div></a>
<a class="thumb-34-5" href="pearl-02/tang-34/can-ho-05-2.html"  data-info="32"><div class="num-block">P2-05</div></a>
<a class="thumb-34-6" href="pearl-02/tang-34/can-ho-06-2.html"  data-info="33"><div class="num-block">P2-06</div></a>
<a class="thumb-34-7" href="pearl-02/tang-34/can-ho-07-2.html"  data-info="34"><div class="num-block">P2-07</div></a>
<a class="thumb-34-8" href="pearl-02/tang-34/can-ho-08-2.html"  data-info="35"><div class="num-block">P2-08</div></a>
<a class="thumb-34-9" href="pearl-02/tang-34/can-ho-09-2.html"  data-info="36"><div class="num-block">P2-09</div></a>
<a class="thumb-34-10" href="pearl-02/tang-34/can-ho-10-2.html"  data-info="37"><div class="num-block">P2-10</div></a>
<a class="thumb-34-11" href="pearl-02/tang-34/can-ho-11-2.html"  data-info="38"><div class="num-block">P2-11</div></a>
<a class="thumb-34-12" href="pearl-02/tang-34/can-ho-12-2.html"  data-info="39"><div class="num-block">P2-12</div></a>
</div>
</div>
<div class="block-bg block34"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="28"><a  href="pearl-02/tang-34/can-ho-01.html" ><h3><strong>P2-01 (A1)</strong> [54.3 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="29"><a  href="pearl-02/tang-34/can-ho-02-2.html" ><h3><strong>P2-02 (C1)</strong> [90.41 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="30"><a  href="pearl-02/tang-34/can-ho-03-2.html" ><h3><strong>P2-03 (C1)</strong> [90.41 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="31"><a  href="pearl-02/tang-34/can-ho-04-2.html" ><h3><strong>P2-04 (A1)</strong> [53.28 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="32"><a  href="pearl-02/tang-34/can-ho-05-2.html" ><h3><strong>P2-05 (B2)</strong> [83.99 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="33"><a  href="pearl-02/tang-34/can-ho-06-2.html" ><h3><strong>P2-06 (C2A)</strong> [104.13 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="34"><a  href="pearl-02/tang-34/can-ho-07-2.html" ><h3><strong>P2-07 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="35"><a  href="pearl-02/tang-34/can-ho-08-2.html" ><h3><strong>P2-08 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="36"><a  href="pearl-02/tang-34/can-ho-09-2.html" ><h3><strong>P2-09 (B1)</strong> [79.02 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="37"><a  href="pearl-02/tang-34/can-ho-10-2.html" ><h3><strong>P2-10 (D1)</strong> [286.22 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="38"><a  href="pearl-02/tang-34/can-ho-11-2.html" ><h3><strong>P2-11 (D1)</strong> [286.23 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="39"><a  href="pearl-02/tang-34/can-ho-12-2.html" ><h3><strong>P2-12 (C3)</strong> [109.71 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
<!--tang 35-->
<div class="colum-box" id="3">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-35-1" href="pearl-02/tang-35/can-ho-01-1.html"  data-info="40"><div class="num-block">P2-01</div></a>
<a class="thumb-35-2" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-02-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-02-3.html%27"  data-info="41"><div class="num-block">P2-02</div></a>
<a class="thumb-35-3" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-03-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-03-3.html%27"  data-info="42"><div class="num-block">P2-03</div></a>
<a class="thumb-35-4" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-04-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-04-3.html%27"  data-info="43"><div class="num-block">P2-04</div></a>
<a class="thumb-35-5" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-05-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-05-3.html%27"  data-info="44"><div class="num-block">P2-05</div></a>
<a class="thumb-35-6" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-06-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-06-3.html%27"  data-info="45"><div class="num-block">P2-06</div></a>
<a class="thumb-35-7" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-07-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-07-3.html%27"  data-info="46"><div class="num-block">P2-07</div></a>
<a class="thumb-35-8" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-08-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-08-3.html%27"  data-info="47"><div class="num-block">P2-08</div></a>
<a class="thumb-35-9" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-09-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-09-3.html%27"  data-info="48"><div class="num-block">P2-09</div></a>
<a class="thumb-35-10" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-10-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-10-3.html%27"  data-info="49"><div class="num-block">P2-10</div></a>
</div>
</div>
<div class="block-bg block35"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="40"><a  href="pearl-02/tang-35/can-ho-01-1.html" ><h3><strong>P2-01 (A1)</strong> [54.3 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="41"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-02-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-02-3.html%27" ><h3><strong>P2-02 (C1)</strong> [90.41 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="42"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-03-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-03-3.html%27" ><h3><strong>P2-03 (C1)</strong> [90.42 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="43"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-04-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-04-3.html%27" ><h3><strong>P2-04 (A1)</strong> [53.28 m<sup>2</sup>, 1 phòng ngủ, 1 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="44"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-05-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-05-3.html%27" ><h3><strong>P2-05 (B2)</strong> [84.03 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="45"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-06-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-06-3.html%27" ><h3><strong>P2-06 (C2A)</strong> [104.13 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="46"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-07-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-07-3.html%27" ><h3><strong>P2-07 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="47"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-08-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-08-3.html%27" ><h3><strong>P2-08 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="48"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-09-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-09-3.html%27" ><h3><strong>P2-09 (D2)</strong> [326.91 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="49"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-10-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-35/can-ho-10-3.html%27" ><h3><strong>P2-10 (D3)</strong> [388.35 m<sup>2</sup>, 5 phòng ngủ, 5 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
<div class="colum-box" id="4">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-36-1" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-01-2.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-01-2.html%27"  data-info="50"><div class="num-block">P2-01</div></a>
<a class="thumb-36-2" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-02-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-02-4.html%27"  data-info="51"><div class="num-block">P2-02</div></a>
<a class="thumb-36-3" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-03-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-03-4.html%27"  data-info="52"><div class="num-block">P2-03</div></a>
<a class="thumb-36-4" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-04-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-04-4.html%27"  data-info="53"><div class="num-block">P2-04</div></a>
<a class="thumb-36-5" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-05-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-05-4.html%27"  data-info="54"><div class="num-block">P2-05</div></a>
<a class="thumb-36-6" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-06-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-06-4.html%27"  data-info="55"><div class="num-block">P2-06</div></a>
<a class="thumb-36-7" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-07-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-07-4.html%27"  data-info="56"><div class="num-block">P2-07</div></a>
<a class="thumb-36-8" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-08-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-08-4.html%27"  data-info="57"><div class="num-block">P2-08</div></a>
</div>
</div>
<div class="block-bg block36"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="50"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-01-2.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-01-2.html%27" ><h3><strong>P2-01 (C4)</strong> [144.71 m<sup>2</sup>, 4 phòng ngủ, 3 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="51"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-02-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-02-4.html%27" ><h3><strong>P2-02 (C4)</strong> [143.68 m<sup>2</sup>, 4 phòng ngủ, 3 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="52"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-03-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-03-4.html%27" ><h3><strong>P2-03 (B2)</strong> [83.99 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="53"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-04-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-04-4.html%27" ><h3><strong>P2-04 (C2A)</strong> [104.14 m<sup>2</sup>, 3 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="54"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-05-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-05-4.html%27" ><h3><strong>P2-05 (B1)</strong> [78.99 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="55"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-06-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-06-4.html%27" ><h3><strong>P2-06 (B1)</strong> [79.01 m<sup>2</sup>, 2 phòng ngủ, 2 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="56"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-07-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-07-4.html%27" ><h3><strong>P2-07 (D2)</strong> [326.91 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="57"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-08-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-36/can-ho-08-4.html%27" ><h3><strong>P2-08 (D3)</strong> [388.35 m<sup>2</sup>, 5 phòng ngủ, 5 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
<div class="colum-box" id="5">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-37-1" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-01-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-01-3.html%27"  data-info="58"><div class="num-block">P2-01</div></a>
<a class="thumb-37-2" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-02-5.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-02-5.html%27"  data-info="59"><div class="num-block">P2-02</div></a>
<a class="thumb-37-3" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-03-5.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-03-5.html%27"  data-info="60"><div class="num-block">P2-03</div></a>
<a class="thumb-37-4" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-04-5.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-04-5.html%27"  data-info="61"><div class="num-block">P2-04</div></a>
</div>
</div>
<div class="block-bg block37"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="58"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-01-3.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-01-3.html%27" ><h3><strong>P2-01 (PH1)</strong> [425.89 m<sup>2</sup>, 5 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="59"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-02-5.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-02-5.html%27" ><h3><strong>P2-02 (PH2)</strong> [313.88 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="60"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-03-5.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-03-5.html%27" ><h3><strong>P2-03 (PH3)</strong> [262.07 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="61"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-04-5.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-37/can-ho-04-5.html%27" ><h3><strong>P2-04 (PH4)</strong> [543.77 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
<div class="colum-box" id="6">
<div class="block">
<div class="compass"></div>
<div class="block-typical">
<div class="block-typical-top">
<a class="thumb-38-1" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-01-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-01-4.html%27"  data-info="62"><div class="num-block">P2-01</div></a>
<a class="thumb-38-2" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-02-6.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-02-6.html%27"  data-info="63"><div class="num-block">P2-02</div></a>
<a class="thumb-38-3" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-03-6.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-03-6.html%27"  data-info="64"><div class="num-block">P2-03</div></a>
<a class="thumb-38-4" href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-04-6.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-04-6.html%27"  data-info="65"><div class="num-block">P2-04</div></a>
</div>
</div>
<div class="block-bg block38"></div>
</div>
<div class="left-plan">
<div class="keyplan"><span>vị trí <strong>PEARL 02</strong></span><img src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/apartment/pearl-02.png"  alt="keyplan"></div>
</div>
<div class="info-block">
<div class="block-name" data-block="62"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-01-4.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-01-4.html%27" ><h3><strong>P2-01 (PH1)</strong> [425.89 m<sup>2</sup>, 5 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="63"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-02-6.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-02-6.html%27" ><h3><strong>P2-02 (PH2)</strong> [313.88 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="64"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-03-6.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-03-6.html%27" ><h3><strong>P2-03 (PH3)</strong> [262.07 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
<div class="block-name" data-block="65"><a  href="javascript:if(confirm(%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-04-6.html  \n\nThis file was not retrieved by Teleport Pro, because it was unavailable, or its retrieval was aborted, or the project was stopped too soon.  \n\nDo you want to open it from the server?%27))window.location=%27http://mydinhpearl.com.vn/thong-tin-can-ho/pearl-02/tang-38/can-ho-04-6.html%27" ><h3><strong>P2-04 (PH4)</strong> [543.77 m<sup>2</sup>, 4 phòng ngủ, 4 WC]</h3><span class="shape"></span></a></div>
</div>
</div>
</div>
<a class="go-back" href="../thong-tin-can-ho.html" ><span></span>Trở về</a>
<a class="link-page-active class-hidden" href="pearl-02.html" ></a>
<div class="shadow"></div>
</div>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>