<?php //template name: Ý tưởng thiết kế ?>
<?php get_header(); ?>
<!--CONTAINER-->
<div class="container">
<div class="title-page"><h1>Giới thiệu dự án</h1></div>
<div class="slider-about"> 
<div class="sub-nav">
<ul>
<li  ><h2>Giới thiệu dự án</h2><a href="gioi-thieu/gioi-thieu-du-an"  data-name="1" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL"></a></li>
<li   ><h2>Quy mô dự án</h2><a href="gioi-thieu/quy-mo-du-an"  data-name="5" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL"></a></li>
<li class="current" ><h2>Ý tưởng thiết kế</h2><a href="gioi-thieu/y-tuong-thiet-ke"  data-name="6" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL"></a></li>
<li  ><h2>Chủ đầu tư</h2><a href="gioi-thieu/chu-dau-tu"  data-name="2" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL"></a></li>
<li ><h2>Đối tác</h2><a href="gioi-thieu/doi-tac"  data-name="4" data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL"></a></li>
</ul>
</div>
<div class="slide-bg">
<div class="item-wrapper">
<div class="item-container" data-hash="1" data-href="gioi-thieu/gioi-thieu-du-an"  data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" >
<div class="bg-picture">
<img class="desktop" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg7.jpg"  alt="Ngọc xanh trong lòng Hà Nội">
<img class="mobile" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg7-m.jpg"  alt="Ngọc xanh trong lòng Hà Nội">
</div>
<div class="content-box color-01">
<h2>Ngọc xanh trong lòng Hà Nội</h2>
<p>Sự hiện diện và phát triển nhanh chóng của các khu đô thị đang dần xóa đi những bức tranh thiên nhiên xanh của Thủ đô. Mỹ Đình Pearl được thiết kế và xây dựng để tạo ra một viên ngọc xanh tươi ngay tại khu đô thị sầm uất phía Tây Hà Nội. Mỹ Đình Pearl không chỉ mang đến cho bạn những căn hộ hiện đại mà còn môi trường sống tiện nghi, trong lành, mang hơi thở của thiên nhiên. Hãy tận hưởng cuộc sống sang trọng, hiện đại, bình yên và hạnh phúc cùng gia đình tại Mỹ Đình Pearl Hà Nội.</p></div>
</div>
<div class="item-container" data-hash="5" data-href="gioi-thieu/quy-mo-du-an"  data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" >
<div class="bg-picture">
<img class="desktop" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg8.jpg"  alt="Tổng quan Dự án Mỹ Đình Pearl">
<img class="mobile" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg8-m.jpg"  alt="Tổng quan Dự án Mỹ Đình Pearl">
</div>
<div class="content-box color-02">
<h2>Tổng quan Dự án Mỹ Đình Pearl</h2>
<p>• Tổng diện tích dự án: 3.8 ha<br>  • Mật độ xây dựng toàn dự án: 24.87%<br> • Diện tích đất khu căn hộ: 15,993 m<sup>2</sup><br> • Mật độ xây dựng khối căn hộ: 18.25%<br> • Tháp căn hộ: 2 tháp cao 38 tầng <br> • Tổng số căn hộ: 984 căn<br>  • Số tầng hầm đỗ xe: 02 hầm</p></div>
</div>
<div class="item-container" data-hash="6" data-href="gioi-thieu/y-tuong-thiet-ke"  data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL" >
<div class="bg-picture">
<img class="desktop" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg9.jpg"  alt="Ý tưởng thiết kế">
<img class="mobile" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg9-m.jpg"  alt="Ý tưởng thiết kế">
</div>
<div class="content-box color-03">
<h2>Ý tưởng thiết kế</h2>
<p>Với ý tưởng tạo ra một tổ ấm đích thực, an lành cho cư dân tại Mỹ Đình Pearl, chúng tôi cùng đơn vị thiết kế đã chăm chút từng chi tiết để tạo nên tổ hợp kiến trúc hiện đại theo tiêu chuẩn quốc tế với những đường nét tinh tế hòa cùng cảnh quan thiên nhiên xanh mát bao quanh, tạo nên một quần thể phát triển hoàn chỉnh và đồng bộ.</p></div>
</div>
<div class="item-container"  data-hash="2" data-href="gioi-thieu/chu-dau-tu"  data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL">
<div class="bg-picture">
<img class="desktop" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg10.jpg"  alt="Chủ đầu tư">
<img class="mobile" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg10-m.jpg"  alt="Chủ đầu tư">
</div>
<div class="content-box-2 color-04">
<h2>Chủ đầu tư</h2>
<div class="box-scroll">
<div class="scrollA">
<p>SSG là tập đoàn đa ngành nghề với lĩnh vực trọng tâm là đầu tư kinh doanh bất đông sản. Những công trình đã khẳng định vị thế và uy tín của Tập đoàn SSG được khách hàng tin tưởng lựa chọn như: <strong>Sài Gòn Pearl</strong>, <strong>Thảo Điền Pearl</strong>, <strong>Sài Gòn Airport Plaza</strong>, <strong>Pearl Plaza</strong>.</p><p> Với phương châm và mục tiêu mang đến dự án bất động sản chất lượng, đáng mơ ước cho khách hàng. Cùng với năng lực xây dựng, kinh nghiệm vững mạnh và tâm huyết của toàn thể Ban lãnh đạo và cán bộ nhân viên, Tập đoàn SSG quyết tâm theo đuổi các mục tiêu sứ mệnh của mình để góp phần thay đổi cảnh quan, quần thể kiến trúc đô thị đồng thời kiến tạo môi trường sống lý tưởng, trở thành giá trị đảm bảo tương lai vững bền cho đời sống cư dân tại mỗi dự án.Tiếp bước những thành công và kinh nghiệm đã có, Tập đoàn SSG tiếp tục đầu tư  Tổ hợp Mỹ Đình Pearl - “viên ngọc mới” của Tập đoàn SSG tại thị trường Hà Nội</p><p>Ngoài lĩnh vực đầu tư Kinh doanh Bất động sản, lĩnh vực Đầu tư phát triển giáo dục và Năng lượng tái tạo cũng là 2 lĩnh vực chính được đầu tư và hoạt động hiểu quả của SSG Group. </p></div>
</div>
<div class="projects">
<div class="item-project">
<?php get_sidebar( 'project' ); ?>
</div>
</div>
</div>
</div>
<div class="item-container"  data-hash="4"  data-href="gioi-thieu/doi-tac"  data-title="MY DINH PEARL" data-description="MY DINH PEARL" data-keyword="MY DINH PEARL">
<div class="bg-picture">
<img class="desktop" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg11.jpg"  alt="Đối tác">
<img class="mobile" src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/background/bg11-m.jpg"  alt="Đối tác">
</div>
<div class="content-box-2 color-05">
<h2>Đối tác</h2>
<div class="box-scroll">
<div class="scrollA">
<div class="group">
<h4>Đơn vị tư vấn thiết kế</h4>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/1.jpg"  alt="ARCHETYPE GROUP">
<span>ARCHETYPE GROUP</span></div>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/2.jpg"  alt="XUÂN MAI CORP">
<span>XUÂN MAI CORP</span></div>
</div>
<div class="group">
<h4>Đơn vị tư vấn kết cấu</h4>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/3.jpg"  alt="VISTA">
<span>VISTA</span></div>
</div>
<div class="group">
<h4>Đơn vị tư vấn cơ điện</h4>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/4.jpg"  alt="AURECON">
<span>AURECON</span></div>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/5.jpg"  alt="SPACE">
<span>SPACE</span></div>
</div>
<div class="group">
<h4>Đơn vị tư vấn thiết kế cảnh quan</h4>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/6.jpg"  alt="STV LANDSCAPE">
<span>STV LANDSCAPE</span></div>
</div>
<div class="group">
<h4>Đơn vị tư vấn thiết kế nội thất</h4>
<div class="partner">
<img  src="<?php echo get_template_directory_uri(); ?>/pictures/catalog/about/partners/7.jpg"  alt="TRANSFORM">
<span>TRANSFORM</span></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!--BOTTOM-->
<?php get_sidebar( 'bottom' ); ?>
<!--BOTTOM-->
</div>
<!--CONTAINER-->
<?php get_footer(); ?>