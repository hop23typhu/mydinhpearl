<div class="bottom">
<div class="bottom-bg"></div>
<!--HOTLINE-->
<div class="hotline">
<a class="call-number" href="tel:<?php echo str_replace(' ', '', print_option('hotline'));  ?>"><?php echo print_option('hotline'); ?></a>
<span class="call"></span>
<a class="call-number" href="tel:<?php echo str_replace(' ', '', print_option('hotline2'));  ?>"><?php echo print_option('hotline2'); ?></a>
</div>
<!--HOTLINE-->
<!--BOTTOM-CENTER-->
<div class="bottom-center">
<!--NEWS-->
<div class="news-icon">
<span class="news"></span>
<a class="go-news" href="<?php bloginfo('wpurl'); ?>/ha-noi-keu-goi-dau-tu-150-000-ty-dong-vao-4-du-an-duong-sat.html" >Sự kiện nổi bật</a>
<g></g>
</div>
<!--NEWS-->
<!--VIDEO-->
<div class="video-icon">
<span class="video"></span>
<a class="play-video" href="javascript:void(0);" data-href="<?php bloginfo('wpurl'); ?>/video">Video</a>
<g></g>
</div>
<!--VIDEO-->
<!--SUBSCRIBE-->
<div class="subscribe-icon">
<span class="subscribe"></span>
<a class="subscribe-link" href="javascript:void(0);">Đăng ký nhận bản tin</a>
<g></g>
</div>
<!--SUBSCRIBE-->
</div>
<!--BOTTOM-CENTER-->
<div class="bottom-text">
<div class="copyright"><?php echo get_theme_mod( 'copy_right' ); ?></div>
</div>
</div>