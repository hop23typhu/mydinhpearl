<!DOCTYPE HTML>
<html lang="vi">
<head>
<?php wp_head(); ?>
<link rel="SHORTCUT ICON" href="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/images/favicon.ico" >
<style type="text/css">
body {background-color:#f3eacc; margin:0; padding:0;font-family: Arial, Helvetica, sans-serif; line-height:1; overflow:hidden }
* { -webkit-box-sizing: border-box;  box-sizing: border-box;}
*, *:before,*:after {-webkit-box-sizing: border-box; box-sizing: border-box;}
.page-not-found { padding:50px; display:block; height:auto; width:90%; max-width:500px; margin:5% auto; text-align:left; background:#efefef; -webkit-border-radius:15px; border-radius:15px;  box-shadow:0 0 40px rgba(0,0,0,0.5); position:relative; z-index:999; }
.page-not-found .img-logo { -webkit-background-size:cover; background-size:cover; background-position:center center; background-repeat:no-repeat; background-image:url(http://mydinhpearl.com.vn/catalog/view/theme/default/images/pattern.png); text-align:center;}
.page-not-found .img-logo img {max-width:100%;}
.page-not-found h1 { font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#333; line-height:28px; }
.page-not-found span { font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:22px; }
.page-not-found a { font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#C60; line-height:22px; }
.page-not-found a:hover { font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#333; line-height:22px; }
#background { position:absolute; top:0; left:0; width:100%; height:100%; background-color:rgba(0,0,0,0.3); }
#background span{ width:100%; height:100%;position:absolute; top:0; left:0; -webkit-background-size:cover; background-size:cover; background-position:center center; background-repeat:no-repeat; z-index:-5}
</style>
</head>
<body>
<div class="page-not-found">
<div class="img-logo"> <img src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/images/logo.png"> </div>
<h1>Không tìm thấy nội dung - 404 !</h1>
<span>Quay lại  <a href="<?php bloginfo('wpurl') ?>">Trang chủ</a></span> </div>
<div id="background"><span style="background-image:url(<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/images/bg-error.jpg)" ></span></div>
</body>
</html>