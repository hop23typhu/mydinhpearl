<?php


function headers_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'headers_session' , array(
	    'title'      => 'Tùy chỉnh đầu trang',
	    'priority'   => 30,
	) );
	//1 tuỳ chỉnh gồm 2 thành phần setting , và control
	

	$wp_customize->add_setting( 'banner_headers' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'control_banner_headers', 
		array(
			'label'      => 'Ảnh banner',
			'section'    => 'headers_session',
			'settings'   => 'banner_headers',
		)
	));


}
add_action( 'customize_register', 'headers_customize_register' );//thực thi hàm
