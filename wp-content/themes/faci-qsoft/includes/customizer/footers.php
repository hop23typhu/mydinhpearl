<?php


function footers_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'footers_session' , array(
	    'title'      => 'Tùy chỉnh chân trang',
	    'priority'   => 31,
	) );
	//1 tuỳ chỉnh gồm 2 thành phần setting , và control
	

	//Quản lý ảnh banner
	$wp_customize->add_setting( 'background_footer' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'control_background_footer', 
		array(
			'label'      => 'Ảnh nền chân trang',
			'section'    => 'footers_session',
			'settings'   => 'background_footer',
		)
	));


}
add_action( 'customize_register', 'footers_customize_register' );//thực thi hàm
