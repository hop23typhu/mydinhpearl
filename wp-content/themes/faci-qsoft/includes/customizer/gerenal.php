<?php

function deeds_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', 'deeds_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 * Js 
 */
function deeds_customize_preview_js() {
	wp_enqueue_script( 'deeds_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'deeds_customize_preview_js' );

function faci_customize_register( $wp_customize ) {
	$wp_customize->add_section( 'faci_session' , array(
	    'title'      => 'Faci tùy chỉnh',
	    'priority'   => 30,
	) );
	//1 tuỳ chỉnh gồm 2 thành phần setting , và control


	$wp_customize->add_setting( 'img_error' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'control_img_error', 
		array(
			'label'      => 'Ảnh lỗi 404',
			'section'    => 'faci_session',
			'settings'   => 'img_error',
		)
	));
	$wp_customize->add_setting( 'img_backtop' , array(
	    'default'     => '',
	) );
	$wp_customize->add_control( new WP_Customize_Image_Control( 
		$wp_customize, 
		'control_img_backtop', 
		array(
			'label'      => 'Ảnh lên trên',
			'section'    => 'faci_session',
			'settings'   => 'img_backtop',
		)
	));
	$wp_customize->add_setting( 'copy_right' , array(
	    'default'     => ''
	) );
	$wp_customize->add_control(
		'control_copy_right', 
		array(
			'label'    => 'Copy right',
			'section'  => 'faci_session',
			'settings' => 'copy_right',
			'type'     => 'textarea',
		)
	);

}
add_action( 'customize_register', 'faci_customize_register' );//thực thi hàm
