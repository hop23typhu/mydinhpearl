<?php  

add_action( 'init', 'cptui_register_my_cpts_item' );
function cptui_register_my_cpts_item() {
	$labels = array(
		"name" => "Products",
		"singular_name" => "Products",
		"menu_name" => "Products",
		"all_items" => "All Products",
		"add_new" => "New product",
		"add_new_item" => "New product",
		"edit" => "Edit product",
		"edit_item" => "Edit product",
		);

	$args = array(
		"labels" => $labels,
		"description" => "My Products",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "item", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-calendar-alt",		
		"supports" => array( "title", "thumbnail" ),		
		"taxonomies" => array( "post_tag" ),		
	);
	register_post_type( "item", $args );

// End of cptui_register_my_cpts_item()
}





add_action( 'init', 'cptui_register_my_cpts_orders' );
function cptui_register_my_cpts_orders() {
	$labels = array(
		"name" => "Orders",
		"singular_name" => "Orders",
		"menu_name" => "Orders",
		"all_items" => "All Orders",
		);

	$args = array(
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"has_archive" => true,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "orders", "with_front" => true ),
		"query_var" => true,
		"menu_icon" => "dashicons-cart",		
		"supports" => array( "title" ),				
	);
	register_post_type( "orders", $args );

// End of cptui_register_my_cpts_orders()
}




add_action( 'init', 'cptui_register_my_taxes_item_category' );
function cptui_register_my_taxes_item_category() {

	$labels = array(
		"name" => "Product category",
		"label" => "Product category",
		"menu_name" => "Product category",
		"all_items" => "Product category",
		);

	$args = array(
		"labels" => $labels,
		"hierarchical" => true,
		"label" => "Product category",
		"show_ui" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'item-category', 'with_front' => true ),
		"show_admin_column" => false,
	);
	register_taxonomy( "item-category", array( "item" ), $args );

// End cptui_register_my_taxes_item_category()
}