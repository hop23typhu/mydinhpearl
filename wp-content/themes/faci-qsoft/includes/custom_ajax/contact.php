<?php  
add_action('wp_ajax_faci_contact', 'faci_contact');
add_action('wp_ajax_nopriv_faci_contact', 'faci_contact');
function faci_contact() {
	/*----------VALIDATION----------*/
	//full name 
	if (isset($_POST['name']) & $_POST['name']!='') $cname = $_POST['name']; else die("no 1");
	//full address 
	if (isset($_POST['address']) & $_POST['address']!='') $caddress = $_POST['address']; else die("no 2");
	//email
	if (isset($_POST['email']) & $_POST['email']!='') $cemail = $_POST['email']; else die("no 3");
	//Message
	if (isset($_POST['comments']) & $_POST['comments']!='') $ccontent = $_POST['comments']; else die("no 4");
	/*----------INSERT DATABASE----------*/
	$post = array(
	    'post_title'    => $cname,
	    'post_content'    => $ccontent,
	    'post_status'   => 'publish',
	    'post_type' => 'contact',
	);
	$ajax_contact_id = wp_insert_post($post);
    add_post_meta($ajax_contact_id, 'cemail', $_POST['email'], true);
    add_post_meta($ajax_contact_id, 'caddress', $_POST['address'], true);
    add_post_meta($ajax_contact_id, 'cphone', $_POST['phone'], true);
    /*----------SEND MAIL----------*/
    $body="<p style='font-weight:100;color:#444'>Họ tên : $cname <br/> Email : $cemail <br/> SĐT : $cphone <br/> Địa chỉ : $caddress</p><p style='font-weight:100;color:#444'>Nội dung : $ccontent</p>";
    if( faci_send_mail( array( get_bloginfo('admin_email') ),"Thư liên hệ",$body) ) 
    die("yes");
	
}
add_action('wp_ajax_faci_contact_register', 'faci_contact_register');
add_action('wp_ajax_nopriv_faci_contact_register', 'faci_contact_register');
function faci_contact_register() {
	/*----------VALIDATION----------*/
	//full name 
	if (isset($_POST['nameregister']) & $_POST['nameregister']!='') $cname = $_POST['nameregister']; else die("no 1");
	//full address 
	if (isset($_POST['addressregister']) & $_POST['addressregister']!='') $caddress = $_POST['addressregister']; else die("no 2");
	//email
	if (isset($_POST['emailregister']) & $_POST['emailregister']!='') $cemail = $_POST['emailregister']; else die("no 3");
	//Message
	if (isset($_POST['commentregister']) & $_POST['commentregister']!='') $ccontent = $_POST['commentregister']; else die("no 4");
	/*----------INSERT DATABASE----------*/
	$post = array(
	    'post_title'    => $cname,
	    'post_content'    => $ccontent,
	    'post_status'   => 'publish',
	    'post_type' => 'contact',
	);
	$ajax_contact_id = wp_insert_post($post);
    add_post_meta($ajax_contact_id, 'cemail', $_POST['emailregister'], true);
    add_post_meta($ajax_contact_id, 'caddress', $_POST['addressregister'], true);
    add_post_meta($ajax_contact_id, 'cphone', $_POST['phoneregister'], true);
    /*----------SEND MAIL----------*/
    $body="<p style='font-weight:100;color:#444'>Họ tên : $cname <br/> Email : $cemail <br/> SĐT : $cphone <br/> Địa chỉ : $caddress</p><p style='font-weight:100;color:#444'>Nội dung : $ccontent</p>";
    if( faci_send_mail( array( get_bloginfo('admin_email') ),"Thư liên hệ",$body) ) 
    die("yes");
	
}