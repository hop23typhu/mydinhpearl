<?php  

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/includes/custom_login/logo.png);
            padding-bottom: 30px;
            background-size: 300px;
            width: 300px;
            height: 250px;
            pointer-events: none;
   			cursor: default;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/includes/custom_login/admin-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );