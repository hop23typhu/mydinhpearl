<?php
function custom_wp_header() {
	$color_general= get_theme_mod('color_general');
	$color_link_tag= get_theme_mod('color_link_tag');
	$nav_bg_color		  = print_option('nav-bg-color');
	$general_font         = print_option('general-font');
	$nav_top_font         = print_option('nav-top-font');
	$nav_cat_font         = print_option('nav-cat-font');
	$footer_font          = print_option('footer-font');
	$footer_widgets_title = print_option('footer-widgets-title');
	$footer_links_font    = print_option('footer-links');
	$logo_font            = print_option('logo-font');
	$h1_font              = print_option('h1-font');
	$h2_font              = print_option('h2-font');
	$h3_font              = print_option('h3-font');
	$h4_font              = print_option('h4-font');
	$h5_font              = print_option('h5-font');
	$h6_font              = print_option('h6-font');
?>
<base href="<?php bloginfo('wpurl'); ?>/">
<meta property="fb:admins" content="<?php echo print_option('facebook-admin-id'); ?>"/>
<meta property="fb:app_id" content="<?php echo print_option('facebook-api-key'); ?>" /> 
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<style>
<?php if(!empty($general_font)): ?>
body {
	font-family: <?php echo $general_font['face']; ?> ;
	font-weight: <?php echo $general_font['style']; ?>;
	font-size: <?php echo $general_font['size'].$general_font['unit']; ?>;
	color: <?php echo $general_font['color']; ?>;
	text-transform: <?php echo $general_font['transform']; ?>;
	background-color: <?php echo print_option('body-bg-color'); ?>
}
.panel h1, .panel h2, .panel h3, .panel h4, .panel h5, .panel h6, .panel p {
	color: <?php echo $general_font['color'] ? $general_font['color'] : '#777'; ?>;
}
<?php endif; ?>
header.top-head {
	background-color: <?php echo print_option('header-bg-color'); ?>
}
nav#top-nav > ul > li:hover, nav#top-nav > ul > li.current-menu-item, #top-nav > ul > li.current_page_item {
	background-color: <?php echo print_option('top-nav-current-color'); ?>;
}
.wrap-cat-nav {
	background-color: <?php echo $nav_bg_color; ?>;
}
#cat-nav > ul > li:hover a, #cat-nav > ul > li.current-menu-item a {
	color: <?php echo $nav_bg_color; ?>;
}
.woocommerce-tabs ul.tabs li.active a, .woocommerce-tabs ul.tabs li:hover a {
	color: <?php echo print_option('button-bg-color');?>;
	border-color: <?php echo print_option('button-bg-color');?>;
}
.amount {
	color: <?php echo print_option('general-price-color'); ?>
}
.block-content span.onsale, .images-wrap span.onsale {
	background-color: <?php echo print_option('onsale-bg-color'); ?>
}
.footer-widgets {
	background-color: <?php echo print_option('footer-bg-color'); ?>
}
.footer-copyright {
	background-color: <?php echo print_option('footer-bg-copyright-color'); ?>
}
a {
	color: <?php echo print_option('general-links-color');?>;
}
a:hover {
	color: <?php echo print_option('general-links-color-hover');?>;
}
button, .button, input[type="submit"], .widget_price_filter .ui-slider .ui-slider-range, .woocommerce form.cart .yith-wcwl-add-to-wishlist a, .progress .meter {
	color: <?php echo print_option('button-text-color');?>;
	background-color: <?php echo print_option('button-bg-color');?>;
	border-color: <?php echo print_option('button-border-color');?>;
}
button:hover, button:focus, .button:hover, .button:focus, input[type="submit"]:hover, input[type="submit"]:focus, .yith-wcwl-add-to-wishlist a:hover, .woocommerce form.cart .yith-wcwl-add-to-wishlist a:hover {
	color: <?php echo print_option('button-text-color-hover', '#FFF');?> !important;
	background-color: <?php echo print_option('button-bg-color-hover');?>;
	border-color: <?php echo print_option('button-border-color-hover');?>;
}
.woocommerce form.cart .add_to_wishlist.loading {
	border-color: <?php echo print_option('button-border-color');?>
}
.block-title {
	font-size: 16px;
	border-color: <?php echo print_option('button-border-color');?>;
}
.widget_price_filter .ui-slider .ui-slider-handle {
	border-color: <?php echo print_option('button-border-color');?>;
}
<?php if(!empty($logo_font)): ?>
h1#top-logo a {
	font-family: <?php echo $logo_font['face']; ?> ;
	font-weight: <?php echo $logo_font['style']; ?>;
	font-size: <?php echo $logo_font['size'].$logo_font['unit']; ?>;
	color: <?php echo $logo_font['color']; ?>;
	text-transform: <?php echo $logo_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($nav_top_font)): ?>
nav#top-nav > ul > li > a {
	font-family: <?php echo $nav_top_font['face']; ?> ;
	font-weight: <?php echo $nav_top_font['style']; ?>;
	font-size: <?php echo $nav_top_font['size'].$nav_top_font['unit']; ?>;
	color: <?php echo $nav_top_font['color']; ?>;
	text-transform: <?php echo $nav_top_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($nav_cat_font)): ?>
nav#cat-nav > ul > li > a {
	font-family: <?php echo $nav_cat_font['face']; ?> ;
	font-weight: <?php echo $nav_cat_font['style']; ?>;
	font-size: <?php echo $nav_cat_font['size'].$nav_cat_font['unit']; ?>;
	color: <?php echo $nav_cat_font['color']; ?>;
	text-transform: <?php echo $nav_cat_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($footer_font)): ?>
.footer-widget .widget-content {
	font-family: <?php echo $footer_font['face']; ?> ;
	font-weight: <?php echo $footer_font['style']; ?>;
	font-size: <?php echo $footer_font['size'].$footer_font['unit']; ?>;
	color: <?php echo $footer_font['color']; ?>;
	text-transform: <?php echo $footer_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($footer_links_font)): ?>
.footer-widget .widget-content a {
	font-family: <?php echo $footer_links_font['face']; ?> ;
	font-weight: <?php echo $footer_links_font['style']; ?>;
	font-size: <?php echo $footer_links_font['size'].$footer_links_font['unit']; ?>;
	color: <?php echo $footer_links_font['color']; ?>;
	text-transform: <?php echo $footer_links_font['transform']; ?>;
}
<?php endif; ?>
.footer-widget a:hover {
	color: <?php echo print_option('footer-links-hover-color'); ?>
}
<?php if(!empty($footer_widgets_title)): ?>
.footer-widget h3 {
	font-family: <?php echo $footer_widgets_title['face']; ?> ;
	font-weight: <?php echo $footer_widgets_title['style']; ?>;
	font-size: <?php echo $footer_widgets_title['size'].$footer_widgets_title['unit']; ?>;
	color: <?php echo $footer_widgets_title['color']; ?>;
	text-transform: <?php echo $footer_widgets_title['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($h1_font)): ?>
h1 {
	font-family: <?php echo $h1_font['face']; ?> ;
	font-weight: <?php echo $h1_font['style']; ?>;
	font-size: <?php echo $h1_font['size'].$h1_font['unit']; ?>;
	color: <?php echo $h1_font['color']; ?>;
	text-transform: <?php echo $h1_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($h2_font)): ?>
h2 {
	font-family: <?php echo $h2_font['face']; ?> ;
	font-weight: <?php echo $h2_font['style']; ?>;
	font-size: <?php echo $h2_font['size'].$h2_font['unit']; ?>;
	color: <?php echo $h2_font['color']; ?>;
	text-transform: <?php echo $h2_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($h3_font)): ?>
h3 {
	font-family: <?php echo $h3_font['face']; ?> ;
	font-weight: <?php echo $h3_font['style']; ?>;
	font-size: <?php echo $h3_font['size'].$h3_font['unit']; ?>;
	color: <?php echo $h3_font['color']; ?>;
	text-transform: <?php echo $h3_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($h4_font)): ?>
h4 {
	font-family: <?php echo $h4_font['face']; ?> ;
	font-weight: <?php echo $h4_font['style']; ?>;
	font-size: <?php echo $h4_font['size'].$h4_font['unit']; ?>;
	color: <?php echo $h4_font['color']; ?>;
	text-transform: <?php echo $h4_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($h5_font)): ?>
h5 {
	font-family: <?php echo $h5_font['face']; ?> ;
	font-weight: <?php echo $h5_font['style']; ?>;
	font-size: <?php echo $h5_font['size'].$h5_font['unit']; ?>;
	color: <?php echo $h5_font['color']; ?>;
	text-transform: <?php echo $h5_font['transform']; ?>;
}
<?php endif; ?>
<?php if(!empty($h6_font)): ?>
h6 {
	font-family: <?php echo $h6_font['face']; ?> ;
	font-weight: <?php echo $h6_font['style']; ?>;
	font-size: <?php echo $h6_font['size'].$h6_font['unit']; ?>;
	color: <?php echo $h6_font['color']; ?>;
	text-transform: <?php echo $h6_font['transform']; ?>;
}
<?php endif; ?>
<?php if(print_option('shop-hide-price') == 'on') : ?>
span.onsale {
	display: none !important;
}
<?php endif; ?>
</style>


<style type="text/css">
	.kun-social{
	  margin-bottom:0px !important;
	  height: 35px;
	  padding-top: 10px;
	}
	.kun-social .button{
	  float:left;
	  margin:0px 10px 10px 0px;
	}
	/* CSS for BACK TO TOP */
	#totop {
		position: fixed;
		right: 30px;
		bottom: 30px;
		display: none;
		outline: none;
	}
	#totop.blue{
	    border: 1px solid #fff;
	    text-align: center;
	    color: #fff;
	    font-size: 3em;
	}
	/* CSS for breadcrumb */
	.breadcrumb{
	  margin-top: 35px;
	  color: black;
	}
	.breadcrumb span:first-child{
	  padding-left: 0px;
	}
	.breadcrumb span{
	  padding:0px 6px;
	  font-size: 14px;
	  font-weight: bold;
	} 

	/* CSS for pagination */
	.wp-pagenavi{
	  float: right;
	}
	.wp-pagenavi .pages{
	    display: none;
	}
	.wp-pagenavi a, .wp-pagenavi a:link, .wp-pagenavi a:visited, .wp-pagenavi span.current{
	    border: 1px solid #CCC;
	    color: #2a6496;
	    background-color: #eee;
	    display: inline;
	    position: relative;
	    float: left;
	    padding: 6px 12px !important;
	    margin-left: -1px;
	    line-height: 1.42857143;
	    color: #428bca;
	    text-decoration: none;
	    background-color: #fff;
	    border: 1px solid #ddd;
	    border-radius: 50% !important;
	    margin: 0 5px;
	}
	.wp-pagenavi span.current{
	    background: #428bca;
		color: #fff;
	}
	.wp-pagenavi a:hover{
	  border: 1px solid #444; 
	}

	/* CSS for image in post */
	.alignnone {
		margin: 5px 20px 20px 0;
	}
	.aligncenter, div.aligncenter {
		display:block;
		margin: 5px auto 5px auto;
	}
	.alignright {
		float:right;
		margin: 5px 0 20px 20px;
	}
	.alignleft {
		float:left;
		margin: 5px 20px 20px 0;
	}
	.aligncenter {
		display: block;
		margin: 5px auto 5px auto;
	}
	a img.alignright {
		float:right;
		margin: 5px 0 20px 20px;
	}
	a img.alignnone {
		margin: 5px 20px 20px 0;
	}
	a img.alignleft {
		float:left;
		margin: 5px 20px 20px 0;
	}
	a img.aligncenter {
		display: block;
		margin-left: auto;
		margin-right: auto
	}
	.wp-caption {
		background: #fff;
		border: 1px solid #f0f0f0;
		max-width: 96%; 
		padding: 5px 3px 10px;
		text-align: center;
	}
	.wp-caption.alignnone {
		margin: 5px 20px 20px 0;
	}
	.wp-caption.alignleft {
		margin: 5px 20px 20px 0;
	}
	.wp-caption.alignright {
		margin: 5px 0 20px 20px;
	}
	.wp-caption img {
		border: 0 none;
		height: auto;
		margin:0;
		max-width: 98.5%;
		padding:0;
		width: auto;
	}
	.wp-caption p.wp-caption-text {
		font-size:11px;
		line-height:17px;
		margin:0;
		padding:0 4px 5px;
	}
</style>
<!-- Custom styles & scripts-->
<?php if(print_option('custom-style-code') != ''): ?>
<style>
<?php echo print_option('custom-style-code'); ?>
</style>
<?php endif; ?>

<?php if(print_option('custom-script-code') != ''): ?>
<script>
<?php echo print_option('custom-script-code'); ?>
</script>
<?php endif; ?>

<?php
}
add_action('wp_head', 'custom_wp_header', 1);
function custom_wp_footer() {
	echo do_shortcode( '[faci_back_to_top]' ); 
?>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			$(window).scroll(function () {
				if ( $(this).scrollTop() > 500 )
					$("#totop").fadeIn();
				else
					$("#totop").fadeOut();
			});

			$("#totop").click(function () {
				$("body,html").animate({ scrollTop: 0 }, 800 );
				return false;
			});
		});
	</script>
	<script type="text/javascript">
	window.___gcfg = {lang: 'vi'};
	(function() {
	var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
	po.src = 'https://apis.google.com/js/platform.js';
	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
	})();
	</script>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=<?php echo print_option('facebook-api-key'); ?>";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<?php 
		if(print_option('google-analytics-code') != ''): 
			echo print_option('google-analytics-code'); 
		endif; 
	?>
<?php
 }
add_action( 'wp_footer', 'custom_wp_footer' );
?>