<?php  
function faci_send_mail($to='',$title='',$body='')
{
	$bodyHTML=
	"
	    <div style='width:100%;background:#e5e5e5;padding:30px 0px;'><table style='margin:0 auto;width:80%;background:#fff;border: 1px solid #990000;padding:0px 20px;padding-bottom:30px'>
	      <tbody>
	        <tr><td style='text-align:center;color:#990000;font-weight:bold;'><h3 style='padding:5px;border-bottom:3px solid green;font-size:27px;font-weight:bold'>".get_bloginfo('name')."</h3></td></tr>
	        <tr>
	          <td style='margin-top:0;margin-bottom:.5em;font-size:18px;font-weight:bold;font-family:sans-serif;padding:5px 20px;line-height:170%;font-size:15px;font-family:arial;color:#666;font-weight:bold'>".$body."</td>
	        </tr>
	        <tr><td style='padding:10px;background:green;color:#fff;font-weight:bold;text-transform:capitalize;text-align:center'><b>".get_bloginfo('name')."<br/>".get_bloginfo('description')."</b></td></tr>
	      </tbody>
	    </table></div>
	";//Content email
    add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));
    if(wp_mail( $to, $title,$bodyHTML ) ) return true;else return false;
}