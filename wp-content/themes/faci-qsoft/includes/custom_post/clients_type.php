<?php
/** Client **/
add_action( 'init', 'create_clients' );
function create_clients() {
    global $themename;
    $labels = array(
        'name'               => __('Khách hàng', $themename),
        'singular_name'      => __('Khách hàng', $themename),
       
    );
 
    register_post_type( 'clients', array(
        'labels'            => $labels,  
        'public'            => false,  
        'show_ui'           => true,
        'show_in_menu'      => true,
        'show_in_nav_menus' => false,
        'rewrite'           => false,
        'supports'          => array('title', 'thumbnail','editor'),
        'menu_icon'=>'dashicons-universal-access-alt'
    ) );
}
?>