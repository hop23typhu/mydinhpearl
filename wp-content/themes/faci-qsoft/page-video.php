<?php //template name:Video ?>
<div class="video-list">
	<div class="close-video"></div> 
	<div class="video-wrap">
		<iframe 
			width="100%" 
			height="100%" 
			src="https://www.youtube.com/embed/<?php echo print_option('ytb'); ?>?rel=0&autoplay=1" 
			frameborder="0" 
			allowfullscreen="">
		</iframe>
	</div>
</div>