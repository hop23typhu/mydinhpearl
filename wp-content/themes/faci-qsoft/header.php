<!DOCTYPE HTML>
<html lang="vi">
<head>
<?php wp_head(); ?>
<link rel="SHORTCUT ICON" href="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/images/favicon.ico" >
<link href="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/css/style.css"  rel="stylesheet" type="text/css">
<link href="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/css/validationEngine.jquery.css"  rel="stylesheet" />
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/history.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/common.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/slide.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/scroll.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/load.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/validate.js" ></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/catalog/view/theme/default/js/jquery.validationEngine.js" ></script>
</head>
<body id="<?php echo set_id_page(); ?>"  >
<div id="contact-loader"></div>
<!--REGISTER-->
<div class="register-form">
<div class="close-form"></div>
<h3>Đăng ký <strong>nhận bản tin</strong></h3>
<form onsubmit="return validateregister();" id="frm_register" name="frm_register" method="post">
<div class="require-col">
<div class="input-text">
<input id="nameregister" name="nameregister" value="Họ và Tên (*)" type="text">
</div>
<div class="input-text">
<input id="emailregister" name="emailregister" value="Email (*)" type="text">
</div>
<div class="input-text">
<input id="phoneregister" name="phoneregister" value="Điện Thoại (*)" type="text">
</div>
<div class="input-text">
<input id="addressregister" name="addressregister" value="Địa Chỉ" type="text">
</div>
<div class="input-area">
<textarea name="commentregister" id="commentregister">Nội dung (*)</textarea>
</div> 
<div class="input-but">
<input id="btn-register-submit" value="GỬI" type="button">
<input  type="hidden" name="action" value="faci_contact_register" >
</div>
</div>
</form>
</div>
<!--REGISTER-->
<script>
function validateregister(){
  hidemsg();
  var flagregister = true;
  var nameregister = checkNull('nameregister', "Vui lòng nhập họ tên!","Họ và Tên (*)",'40','-270');
  var commentregister = checkNull('commentregister', "Vui lòng nhập nội dung!","Nội dung (*)",'40','-270');
  var phoneregister = checkNull('phoneregister', "Điện thoại không hợp lệ!","Điện Thoại (*)",'40','-270');
  var emailregister = checkMail('emailregister', "Email không hợp lệ!","Email (*)",'40','-270');  
  if(!nameregister || !commentregister || !phoneregister || !emailregister ){
    flagregister = false;
    setTimeout(hideerror,5000);
  }
  return flagregister;
}


  $('#btn-register-submit').click(function(){   
    if(validateregister()==true){
      $("#contact-loader").show();
      var datapost = $('#frm_register').serialize(); 
      
      $.ajax({  
        type: "POST",  
        url: "<?php echo admin_url('admin-ajax.php'); ?>", 
        data: datapost,
        success: function(success) { 
        $("#contact-loader").hide();
        $('.contact-success').remove();
        alert(success);
        if(success=='yes'){
          document.getElementById('frm_register').reset();
          $('.close-form').trigger('click');
          $('#contact-loader').after("<div  class='contact-success color-blue'>Thông tin đăng ký của Bạn được gửi hoàn tất! Cảm ơn Bạn!</div>");              
        }else{
          $('#contact-loader').after("<div  class='contact-success color-red'>Thông tin đăng ký của bạn không gửi được, vui lòng kiểm tra lại!</div>");
        }
        $('html, body').animate({scrollTop:0}, 'slow');
        setTimeout(hidemsg,5000);
        }  
      });  //end ajax 
      return false; 
    }//end if 
    $(".formError").click(function(){
      $(this).hide(); 
    });
    return false;     
  });
  $("#phoneregister").numeric();
  $('#frm_register').keydown(function(e) {
        if(!$("textarea").is(":focus")){
    if (e.keyCode == 13) {
       $('#btn-register-submit').trigger('click');
        }
    }
    });
</script>
<!--LOAD-PAGE-->
<div class="all-pics"></div>
<div class="all-album"></div>
<div class="allvideo"></div>
<div class="overlay-album"></div>
<div class="overlay-video"></div>
<div class="overlay-dark"></div>
<!--LOAD-PAGE-->
<!--LOGO-->
<a href="<?php bloginfo("wpurl"); ?>"><div class="logo vi"></div></a>
<!--LOGO--><div class="nav-click"></div>
<!--TOP-->
<div class="top">
<!--NAVIGATION-->
  <?php get_sidebar( 'menu' ); ?>
<!--NAVIGATION--></div>
<!--TOP-->
<!--RIGHT-->
<div class="right">
<!--SOCIAL-->
<div class="social">
<ul>
<li><a href=" <?php echo print_option('facebook-url'); ?> "  target="_blank" class="facebook"></a></li>
<li><a href=" <?php echo print_option('google-plus-url'); ?> "  target="_blank" class="google-plus"></a></li>
</ul>
</div>
<!--SOCIAL-->
</div>
<!--RIGHT-->
<!--SLOGAN-->
<div class="slogan vi"></div>
<!--SLOGAN-->
<div class="project-details"></div>
<?php if(is_page_template( 'page-facilities.php' )){ ?>
<!--SHOW BOX-->
<div class="info-facilities">
<div class="facilities-name" data-faci="01"><a  href="javascript:void(0);"><h3><span class="co-01">01</span>Lối vào</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="02"><a  href="javascript:void(0);"><h3><span class="co-02">02</span>Nhà bảo vệ</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="03"><a  href="javascript:void(0);"><h3><span class="co-03">03</span>Lối ra vào tầng hầm</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="04"><a  href="javascript:void(0);"><h3><span class="co-04">04</span>Lối vào khu căn hộ</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="05"><a  href="javascript:void(0);"><h3><span class="co-05">05</span>Vườn hoa</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="06"><a  href="javascript:void(0);"><h3><span class="co-06">06</span>Khu vực kinh doanh ngoài trời</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="07"><a  href="javascript:void(0);"><h3><span class="co-07">07</span>Khu vực nghỉ chân</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="08"><a  href="javascript:void(0);"><h3><span class="co-08">08</span>Khu vui chơi trẻ em</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="09"><a  href="javascript:void(0);"><h3><span class="co-09">09</span>Vườn hoa đào</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="10"><a  href="javascript:void(0);"><h3><span class="co-10">10</span>Sân tập Golf</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="11"><a  href="javascript:void(0);"><h3><span class="co-11">11</span>Cafe ngoài trời</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="12"><a  href="javascript:void(0);"><h3><span class="co-12">12</span>Không gian mở đa chức năng</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="13"><a  href="javascript:void(0);"><h3><span class="co-13">13</span>Khu vực nướng ngoài trời</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="14"><a  href="javascript:void(0);"><h3><span class="co-14">14</span>Khu vực thể dục thể thao ngoài trời</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="15"><a  href="javascript:void(0);"><h3><span class="co-15">15</span>Sân Tennis</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="16"><a  href="javascript:void(0);"><h3><span class="co-16">16</span>Hồ bơi người lớn</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="17"><a  href="javascript:void(0);"><h3><span class="co-17">17</span>Hồ bơi trẻ em</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="18"><a  href="javascript:void(0);"><h3><span class="co-18">18</span>Quầy giải khát hồ bơi</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="19"><a  href="javascript:void(0);"><h3><span class="co-19">19</span>Sàn phơi nắng hồ bơi</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="20"><a  href="javascript:void(0);"><h3><span class="co-20">20</span>Bể sục</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="21"><a  href="javascript:void(0);"><h3><span class="co-21">21</span>Lối thoát hiểm</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="22"><a  href="javascript:void(0);"><h3><span class="co-22">22</span>Thang DV hồ bơi &amp; thoát hiểm tầng hầm</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="23"><a  href="javascript:void(0);"><h3><span class="co-23">23</span>Bãi đậu xe</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="24"><a  href="javascript:void(0);"><h3><span class="co-24">24</span>Cafe</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="25"><a  href="javascript:void(0);"><h3><span class="co-25">25</span>Gym &amp; Spa</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="26"><a  href="javascript:void(0);"><h3><span class="co-26">26</span>Nhà trẻ</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="27"><a  href="javascript:void(0);"><h3><span class="co-27">27</span>Trực phòng cháy chữa cháy</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="28"><a  href="javascript:void(0);"><h3><span class="co-28">28</span>Shop</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="29"><a  href="javascript:void(0);"><h3><span class="co-29">29</span>Shop</h3><span class="shape"></span></a></div>
<div class="facilities-name" data-faci="30"><a  href="javascript:void(0);"><h3><span class="co-30">30</span>Sinh hoạt cộng đồng</h3><span class="shape"></span></a></div>
</div> 
<!--SHOW BOX-->
<?php } ?>