<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mydinhpearl');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'TDoPErFM89Lx&.~-.!}!6F&:S$b}nF${5GDh,C)>TnCYtqE2yU3 HF,$H]boAa`5');
define('SECURE_AUTH_KEY',  'HBa1>maus8p.M1/{nQ>$.q A 7#G2S`$zj~DvYWUq&y+.1ysV&K+80JjcR0( /8`');
define('LOGGED_IN_KEY',    '`VNnNTxKh5zcE!H|D)Q!w<iaQUx3U j;.rs{09&zQb^r@%uz^/~~u+9TCd/2@#47');
define('NONCE_KEY',        'p7(4uRNtbsS!Q4_twn-I)2o)frTk.~/Tnl>Ahn:@VRT]m9L}UOpZ*~dTtO,xJ4,H');
define('AUTH_SALT',        'LB@dn+iI{$Dq;_eTvI_qMhWn?f2*Ll:#mF%N:}fPI*t<T16W}@8Ah2Ock/2FAfiw');
define('SECURE_AUTH_SALT', 'P#1qjD4s.)IgCG`0h?^B],nQ%h1x3*cTQIIl2-:WZwkU}A`lpsF+2!8oE(wojj+u');
define('LOGGED_IN_SALT',   '~Ipx6V#Bu1q.3^+vb_kL2:HoQc5QLk,ZVYLYqQU9PRK:;q2,0cfNFQVGwB<uSBrk');
define('NONCE_SALT',       ',t`o%M2mS5U!:#>55:Rj<[E]8>%lwMd-s XNKnsl`q3P85Fv,(^&ig`q@Dj)+y[T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
